package statsView;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import model.PlayerName;

public class StatsView extends VBox {
    private TableView<PlayerName> table;
    private TableColumn<PlayerName, String> nameColumn;
    private TableColumn<PlayerName, Integer> scoreColumn;
    private TableColumn<PlayerName, Integer> numberRolled;
    private final ObservableList<PlayerName> stats = FXCollections.observableArrayList();

    public StatsView() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        table = new TableView<>();
        nameColumn = new TableColumn<>("Name");
        scoreColumn = new TableColumn<>("Score");
        numberRolled = new TableColumn<>("RandomNumber");

    }

    private void layoutNodes() {
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameColumn.setMinWidth(300);
        scoreColumn.setCellValueFactory(new PropertyValueFactory<>("score"));
        scoreColumn.setMinWidth(300);
        numberRolled.setCellValueFactory(new PropertyValueFactory<>("randomNumber"));
        numberRolled.setMinWidth(300);
        getChildren().addAll(table);
        table.setItems(getTable());
        table.getColumns().addAll(nameColumn, scoreColumn, numberRolled);
    }

    public ObservableList<PlayerName> getTable() {
        return stats;
    }

    public TableView<PlayerName> getTableNames(){
        return table;
    }

}
