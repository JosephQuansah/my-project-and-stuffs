import gameScreen.GamePresenter1;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.PlayerName;
import gameScreen.GameView;


public class Main extends Application {

    @Override
    public void start(Stage stage) {

        PlayerName model = new PlayerName("Anyone",4);
        GameView view = new GameView();
        GamePresenter1 p = new GamePresenter1(view);
        stage.setTitle("OwareGame");
        stage.sizeToScene();
        stage.setScene(new Scene(view));
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
