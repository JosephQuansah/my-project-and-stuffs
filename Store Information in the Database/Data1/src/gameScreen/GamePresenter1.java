package gameScreen;

import model.PlayerName;
import view.GameView;

public class GamePresenter1 {
    private gameScreen.GameView view;

    public GamePresenter1(gameScreen.GameView view){
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {

    }

    private void updateView() {
     view.drawBoard();
     view.boardHoles();
     view.drawMarbles();
     view.drawMarbles2();
//     addScenes();
    }

    void addScenes(){
        PlayerName model = new PlayerName("Anyone",4);
        GameView view = new GameView();
        view.GamePresenter1 p = new view.GamePresenter1(view, model);
        view.getScene().setRoot(this.view);
        view.getScene().getWindow().getScene();
    }
}
