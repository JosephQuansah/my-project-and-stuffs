package gameScreen;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

public class GameView extends BorderPane {
    private Canvas canvas;
    private GraphicsContext graphicsContext;

    public GameView() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        canvas = new Canvas(1530, 830);
        graphicsContext = canvas.getGraphicsContext2D();
    }

    private void layoutNodes() {
        setCenter(canvas);
    }

    void drawBoard() {
        graphicsContext.setFill(Color.BURLYWOOD);
        graphicsContext.fillRoundRect(100, 200, 1300, 500, 500, 1300);
        graphicsContext.strokeRoundRect(100, 200, 1300, 500, 500, 1300);
    }

    void boardHoles() {
        graphicsContext.setFill(Color.GRAY);
        graphicsContext.fillOval(250, 250, 160, 160);
        graphicsContext.fillOval(450, 250, 160, 160);
        graphicsContext.fillOval(650, 250, 160, 160);
        graphicsContext.fillOval(850, 250, 160, 160);
        graphicsContext.fillOval(1050, 250, 160, 160);
        graphicsContext.fillOval(250, 480, 160, 160);
        graphicsContext.fillOval(450, 480, 160, 160);
        graphicsContext.fillOval(650, 480, 160, 160);
        graphicsContext.fillOval(850, 480, 160, 160);
        graphicsContext.fillOval(1050, 480, 160, 160);
    }

    void drawMarbles() {
        graphicsContext.setFill(Color.OLIVEDRAB);
        graphicsContext.fillOval(23, 23, 60, 60);
        //        graphicsContext.strokeOval();
    }

    void drawMarbles2(){
        graphicsContext.setFill(Color.TURQUOISE);
        graphicsContext.fillOval(23,650,60,60);
    }
}
