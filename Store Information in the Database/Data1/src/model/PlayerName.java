package model;

import java.util.Random;

public class PlayerName {
    private String name;
    private int score;
    private final Random random = new Random();
    private final int randomNumber;
    private int number = 0;

    public PlayerName(String name, int score) {
        this.name = name;
        this.score = score;
        this.randomNumber = random.nextInt(4) + 1;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void scorePlayer() {
        number += randomNumber;
        if ( number > 2) {
            score += 15;
        } else {
            score -= 15;
        }
    }

    public int getScore(){
        return score;
    }

    public int getRandomNumber(){
        return randomNumber;
    }

    public String getPlayer2Name() {
        return "CPU";
    }
}
