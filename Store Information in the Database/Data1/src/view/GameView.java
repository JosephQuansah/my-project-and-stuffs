package view;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import org.w3c.dom.Text;


public class GameView extends BorderPane {
    private TextField field;
    private Button play;
    private Button close;
    private Label nameLabel;
    private HBox buttons;
    private HBox nameField;

    public GameView() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        field = new TextField();
        nameLabel = new Label("Name: ");
        play = new Button("Play");
        close = new Button("Close");
        buttons = new HBox();
        nameField = new HBox();
    }

    private void layoutNodes() {
        nameField.getChildren().addAll(nameLabel, field);
        setCenter(nameField);
        buttons.getChildren().addAll(play, close);
        setBottom(buttons);
    }

    public TextField getField(){
        return field;
    }

    public Button getPlayButton(){
        return play;
    }

    public Button getCloseButton(){
        return close;
    }

}
