package view;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.PlayerName;
import statsView.StatsView;

public class GamePresenter1 {
    private final GameView view;
    private final PlayerName model;
    private final StatsView statsView = new StatsView();

    public GamePresenter1(GameView view, PlayerName model) {
        this.view = view;
        this.model = model;
        addEventHandlers();
        updateViews();
    }

    private void addEventHandlers() {
        view.getPlayButton().setOnAction(event -> {
            String name = view.getField().getText();
            model.setName(name);
            model.scorePlayer();
            statsView.getTable().add(new PlayerName(model.getName(),model.getScore()));
            statsView.getTable().add(new PlayerName(model.getPlayer2Name(), model.getScore()));
            stats();
        });
        view.getCloseButton().setOnAction(event -> {
            Platform.exit();
        });
        updateViews();
    }

    private void updateViews() {
        String name = view.getField().getText();
        model.setName(name);
        model.scorePlayer();
    }

    private void stats(){
        Stage stage = new Stage();
        stage.setTitle("Game Statistics");
        stage.setScene(new Scene(statsView));
        stage.show();
    }

}
