package allViews.userInformation;

import allModels.game.gameFolder.GameModel;
import allModels.statistics.StatisticsModel;
import allModels.userInformation.Player;
import allViews.gameScreen.GamePresenter;
import allViews.gameScreen.GameScreen;

public class PlayerOnePresenter {
    private PlayerOne view;
    private Player model;
    private GameScreen gameView = new GameScreen();
    private PlayerTwo playerTwoView = new PlayerTwo();
    private GameModel game = new GameModel();
    
    public PlayerOnePresenter(PlayerOne view, Player model) {
        this.view = view;
        this.model = model;
        addEventsHandlers();
        updateView();
    }
    
    private void addEventsHandlers() {
        view.getPlayerOneButton().setOnAction(e -> {
            String name = view.getField().getText();
            model.setName(name);
            gameView.getPlayerOneName().setText(name);
            GameScreen();
        });
        view.getPlayerTwoButton().setOnAction(e -> {
            PlayerTwoScreen();
        });
        playerTwoView.getPlayerStartGame().setOnAction(e -> {
            String name = view.getField().getText();
            model.setName(name);
            gameView.getPlayerOneName().setText(name);

            String name2 = playerTwoView.getField().getText();
            model.setName(name2);
            gameView.getPlayerTwoName().setText(name2);
            GameScreen2();
            
            StatisticsModel statsModel = new StatisticsModel(name, 3, 400, game.getRoll());
            statsModel.Database();

        });
        updateView();
    }

    private void updateView() {
     
    }

    void GameScreen() {
        GameModel gameModel = new GameModel();
        new GamePresenter(gameView, gameModel);
        view.getScene().setRoot(gameView);
    }

    void GameScreen2() {
        GameModel gameModel = new GameModel();
        new GamePresenter(gameView, gameModel);
        playerTwoView.getScene().setRoot(gameView);
    }

    void PlayerTwoScreen() {
        view.getScene().setRoot(playerTwoView);
    }

}
