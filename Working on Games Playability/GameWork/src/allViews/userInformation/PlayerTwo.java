package allViews.userInformation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class PlayerTwo extends BorderPane {
    private Button playerStartGame;
    private TextField playerName;
    private HBox box;
    private HBox box1;
    private VBox box2;
    private Label texts;
    private Label heading;

    public PlayerTwo() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        playerStartGame = new Button("Start Game");
        playerName = new TextField();
        box = new HBox();
        box1 = new HBox();
        box2 = new VBox();
        texts = new Label("Enter your name: ");
        heading = new Label("Player Two User Information");
    }

    private void layoutNodes() {
        setTop(heading);
        heading.setFont(new Font(30));
        texts.setFont(new Font(18));
        playerStartGame.setPrefSize(200, 80);
        setAlignment(heading, Pos.CENTER);
        playerName.setMaxWidth(500);
        box1.getChildren().addAll(texts, playerName);
        box.getChildren().add(playerStartGame);
        box2.getChildren().addAll(box1, box);
        setCenter(box2);
        box2.setPadding(new Insets(250, 180, 180, 650));
        box2.setSpacing(20);
        box.setSpacing(20);
    }

    Button getPlayerStartGame() {
        return playerStartGame;
    }

    public TextField getField() {
        return playerName;
    }

}
