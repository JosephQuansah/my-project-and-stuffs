package allViews.gameStatistics;

import allModels.statistics.StatisticsModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class GameStatistics extends VBox {
    private TableView<StatisticsModel> table;
    private TableColumn<StatisticsModel, String> nameColumn;
    private TableColumn<StatisticsModel, Integer> rankColumn;
    private TableColumn<StatisticsModel, Integer> scoreColumn;
    private TableColumn<StatisticsModel, Integer> rolledDiceColumn;

    public GameStatistics() {
        InitializeNodes();
        layoutNodes();
    }

    private void InitializeNodes() {
        table = new TableView<>();
        nameColumn = new TableColumn<>("Name");
        rankColumn = new TableColumn<>("Ranking");
        scoreColumn = new TableColumn<>("Score");
        rolledDiceColumn = new TableColumn<>("DiceRolled");
    }

    /**
     * Here we are laying out the objects on the view
     */
    private void layoutNodes() {
        try {
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameColumn.setMinWidth(200);
        rankColumn.setCellValueFactory(new PropertyValueFactory<>("rank"));
        rankColumn.setMinWidth(100);
        scoreColumn.setCellValueFactory(new PropertyValueFactory<>("score"));
        scoreColumn.setMinWidth(100);
        rolledDiceColumn.setCellValueFactory(new PropertyValueFactory<>("diceRolled"));
        rolledDiceColumn.setMinWidth(100);

        getChildren().addAll(table);
        table.setItems(getTable());
     
            table.getColumns().addAll(nameColumn, rankColumn, scoreColumn, rolledDiceColumn);
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }

    public ObservableList<StatisticsModel> getTable() {
        ObservableList<StatisticsModel> stats = FXCollections.observableArrayList();
        stats.add(new StatisticsModel("Joseph", 3, 150, 4));
        stats.add(new StatisticsModel("Bernard", 3, 50, 2));
        stats.add(new StatisticsModel("Lawrence", 3, 100, 3));
        stats.add(new StatisticsModel("Daniel", 3, 150, 3));
        stats.add(new StatisticsModel("Jef", 3, 450, 1));
        stats.add(new StatisticsModel("Assam", 3, 350, 1));
        return stats;
    }
}
