package allViews.gameStatistics;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ViewGameStats extends Application{

    @Override
    public void start(Stage r) throws Exception {
        GameStatistics statistics = new GameStatistics();
        r.setScene(new Scene(statistics));
        r.setTitle("Game Statistics");
        r.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}
