package allViews.gameScreen;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class GameScreen extends BorderPane {
    private Button backButton;
    private Canvas canvas;
    private GraphicsContext g;
    private Label playerOne;
    private Label playerTwo;
    private Label nullInfo;
    private Image boardImage;
    private ImageView dice1;
    private ImageView dice2;
    private HBox top;
    private HBox bottom;



    public GameScreen() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        backButton = new Button("Back");
        canvas = new Canvas(980,800);
        g = canvas.getGraphicsContext2D();
        playerOne = new Label();
        playerTwo = new Label();
        nullInfo = new Label("                                                           ");
        boardImage = new Image("resources/gameBoard.png");
        dice1 = new ImageView("resources/Bdice1.png");
        dice2 = new ImageView();
        top = new HBox();
        bottom = new HBox();
    }

    private void layoutNodes() 
{
        dice1.setFitWidth(100);
        dice1.setFitHeight(100);
        dice2.setFitWidth(100);
        dice2.setFitHeight(100);
        setCenter(canvas);
        backButton.setPrefSize(200, 50);
        setLeft(backButton);
        top.getChildren().addAll(backButton,playerOne,dice1);
        bottom.getChildren ().addAll(nullInfo,playerTwo,dice2);
        playerOne.setFont(new Font(20));
        playerTwo.setFont(new Font(20));
        top.setSpacing(750);
        bottom.setSpacing(750);
        setTop(top);
        setBottom(bottom);
    }

    void playerOneDiscs(int column, int row) {
       g.setFill(Color.RED);
        g.fillOval(row, column, 65, 65);
    }

    void playerTwoDiscs(int column, int row) {
        g.setFill(Color.BLUE);
        g.fillOval(row, column, 65, 65);
    }

    void drawBoard(){
        g.drawImage(boardImage,0,150,950,400);
    }

     Button getBackButton(){
        return backButton;
    }

    public Label getPlayerOneName(){
        return playerOne;
    }

    public Label getPlayerTwoName(){
        return playerTwo;
    }

    ImageView getDice1(){
        return dice1;
    }

    ImageView getDice2(){
        return dice2;
    }
}