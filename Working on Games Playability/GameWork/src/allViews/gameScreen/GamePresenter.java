package allViews.gameScreen;

// import allModels.game.DiscsClass;
import allModels.game.gameFolder.Dice;
import allModels.game.gameFolder.GameModel;
import allViews.userInformation.PlayerOne;
import javafx.scene.image.Image;

public class GamePresenter {
    private GameScreen view;
    private GameModel model = new GameModel();
    private Dice d = new Dice();


    private int sum = 0;
    private int sum2 = 0;

    public void translate2() {
        if (sum2 < 16) {
            if (model.getIndex2() == 730 && model.getRoll() >= 3 || model.getIndex2() == 845 && model.getRoll() >= 4) {
                sum2 = 0;
            } else {
                if (sum2 < 5 || sum2 > 13) {
                    view.playerTwoDiscs(340,model.getIndex2());
                } else {
                    view.playerTwoDiscs(255, model.getIndex2());
                }
                System.out.println("second roll: "+sum2);
            }
        } else {
            sum2 = 0;
        }
        model.finish(sum2);
    }

    public void translate() {
        if (sum < 16) {
            if (model.getIndex() == 730 && d.getRoll() >= 3 || model.getIndex() == 845 && d.getRoll() >= 4) {
                sum = 0;
            } else {
                if (sum < 5 || sum > 13) {
                    view.playerOneDiscs(165, model.getIndex());
                } else {
                    view.playerOneDiscs(255, model.getIndex());
                }
                System.out.println(sum);
            }
        } else {
            sum = 0;
        }
        model.finish(sum);
    }

    public GamePresenter(GameScreen view, GameModel model1) {
        this.view = view;
        this.model = model1;
        addEventsHandlers();
        updateViews();
    }

    private void addEventsHandlers() {
        view.getBackButton().setOnAction(e -> {
            returnButton();
        });

        view.getDice1().setOnMouseClicked(e -> {
            view.drawBoard();
            view.getDice1().setImage(new Image("resources/Bdice" + d.getRoll() + ".png"));
            model.setPieces(sum = sum + d.getRoll());
            translate();
            translate2();
        });
        view.getDice2().setOnMouseClicked(e -> {
            view.getDice1().setImage(new Image("resources/Bdice" + d.getRoll() + ".png"));
            view.drawBoard();
            model.setPieces(sum = sum + d.getRoll());
           translate2();
           translate();
        });
        updateViews();
    }

    private void updateViews() {
        view.drawBoard();
        view.playerTwoDiscs(30, 450);
        model.setPieces(sum = sum + d.getRoll());
        if(sum < 4 || sum  >13){
            view.playerOneDiscs(165,model.getIndex());
        }else{
            view.playerOneDiscs(315,model.getIndex());
        }
    }

    void returnButton() {
        PlayerOne backview = new PlayerOne();
        view.getScene().setRoot(backview);
    }
}
