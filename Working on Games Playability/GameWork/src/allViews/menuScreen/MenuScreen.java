package allViews.menuScreen;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class MenuScreen extends BorderPane {
    private Label gameTitle;
    private Button startGameButton;
    private Button instructionsButton;
    private Button settingButton;
    private Button exitGameButton;
    private VBox box;

    public MenuScreen() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        gameTitle = new Label("Welcome to Royal Game of UR");
        startGameButton = new Button("Start Game");
        instructionsButton = new Button("Instructions");
        settingButton = new Button("Setting");
        exitGameButton = new Button("Exit Game");
        box = new VBox();
    }

    private void layoutNodes() {
        gameTitle.setFont(new Font(30));
        setTop(gameTitle);
        startGameButton.setPrefSize(300,50);
        instructionsButton.setPrefSize(300,50);
        settingButton.setPrefSize(300,50);
        exitGameButton.setPrefSize(300,50);
        box.getChildren().addAll(startGameButton,instructionsButton,settingButton,exitGameButton);
        box.setPadding(new Insets(250,180,180,800));
        box.setSpacing(20);
        setCenter(box);
        setAlignment(box,Pos.CENTER);
        setAlignment(gameTitle,Pos.CENTER);
    }

    Button getStartGameButton() {
        return startGameButton;
    }

    Button getInstructionsButton() {
        return instructionsButton;
    }

    Button getSettingButton() {
        return settingButton;
    }

    Button getExitButton() {
        return exitGameButton;
    }
}
