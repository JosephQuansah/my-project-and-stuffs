package allViews.menuScreen;

import allModels.userInformation.Player;
import allViews.gameStatistics.GameStatistics;
import allViews.userInformation.PlayerOne;
import allViews.userInformation.PlayerOnePresenter;

public class MenuPresenter {
    private MenuScreen view;
    // private MenuModel model;
    private PlayerOne playerView = new PlayerOne();
    private GameStatistics statsView = new GameStatistics();

    public MenuPresenter(MenuScreen view) {
        this.view = view;
        addEventsHandlers();
        updateViews();
    }

    private void addEventsHandlers() {

        view.getStartGameButton().setOnAction(e -> {
            userInformationScreen();
        });

        view.getInstructionsButton().setOnAction(e -> {

        });

        view.getSettingButton().setOnAction(e -> {

        });

        view.getExitButton().setOnAction(e -> {
            gameStatistics();
        });
        updateViews();

    }

    private void updateViews() {
     
    }

    void userInformationScreen() {
        Player model = new Player("Joseph");
        new PlayerOnePresenter(playerView, model);
        view.getScene().setRoot(playerView);
    }

    void gameStatistics(){
        view.getScene().setRoot(statsView);
    }
}
