
import allViews.menuScreen.MenuPresenter;
import allViews.menuScreen.MenuScreen;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class GameMain extends Application {

  @Override
  public void start(Stage a) throws Exception {
    MenuScreen view = new MenuScreen();
    MenuPresenter p = new MenuPresenter(view);
    a.setTitle("RoyalGameOfUr");
    a.setWidth(1950);
    a.setHeight(1090);
    a.setFullScreen(false);
    a.setResizable(false);
    a.getIcons().add(new Image("resources/istockphoto-935974308-612x612.jpg"));
    a.setScene(new Scene(view));
    a.show();
  }

  public static void main(String[] args) {
    launch(args);
  }

}
