package allModels.statistics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class StatisticsModel {
    private String name;
    private int rank;
    private int score;
    private int diceRolled;

    public StatisticsModel(String name, int ranking, int score, int rolledDice) {
        this.name = name;
        this.rank = ranking;
        this.score = score;
        this.diceRolled = rolledDice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getRank() {
        return rank;
    }

    public int getScore() {
        return score;
    }

    public int getDiceRolled() {
        return diceRolled;
    }

    public void Database() {
        String username = "JDBWORK";
        String password = "JeP2020-2023A";
        try {
            Connection con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@jbwork_medium?TNS_ADMIN=C:/Users/empha/Desktop/projects/Vsprojects/wallet_JBWORK",
                    username, password);
            Statement statement = con.createStatement();
            statement.executeUpdate(
                    "Insert into royalGame values('" + getName() + "'," + getScore() + "," + getDiceRolled() + ")");

            ResultSet rs = statement.executeQuery("SELECT * from royalGame ");

            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(1) + " " + rs.getString(1));
            }
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // public static void main(String[] args) {
    // model.Database();
    // }

}
