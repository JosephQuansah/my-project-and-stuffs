package allModels.game;

public class PlayerClass {
    private final String name;
    private final Die die;
    private final DiscsClass[] discs;

    public PlayerClass(String name, DiscsClass[] discs){
        die = new Die();
        this.discs = discs;
        this.name = name;
    }

    public DiscsClass[] getDiscs(){
        return discs;
    }

    public String getName(){
        return name;
    }

    public int takeTurn(){
        int roll = die.getRollDice();
        return roll;
    }
}
