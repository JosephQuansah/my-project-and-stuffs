package allModels.game;

import java.util.Random;

public class Die {
    private final Random random = new Random();

    public int getRollDice() {
        return random.nextInt(4) + 1;
    }
}
