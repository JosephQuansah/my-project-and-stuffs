package allModels.game.gameFolder;

public class Disc {
    private final int numberOfDiscs = 7;
    private int savedDiscs;
    private int discPosition;

    public int getNumberOfDiscs(){
        return numberOfDiscs;
    }

    public int getSavedDiscs(){
        return savedDiscs;
    }

    public int getDiscPosition(){
        return discPosition;
    }

    public void setDiscPosition(int discPosition){
        this.discPosition = discPosition;
    }
}
