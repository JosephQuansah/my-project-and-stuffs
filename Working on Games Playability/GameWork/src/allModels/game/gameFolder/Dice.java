package allModels.game.gameFolder;

import java.util.Random;

public class Dice {
    private Random r = new Random();
    
    public int getRoll(){
        return r.nextInt(3)+1;
    }
}
