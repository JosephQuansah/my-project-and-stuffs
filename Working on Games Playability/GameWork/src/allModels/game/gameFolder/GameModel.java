package allModels.game.gameFolder;

import java.util.Random;

public class GameModel {
    private final Random random = new Random();
    private int index = 0;
    private int index2 = 0;
    private int oldPos;
    private int newPos;
    private int score;
    private int discSaved;
    
    private int[][] path = new int[][]{
            {380, 270, 150, 30, 500, 615,615, 845, 730},
            {30, 270, 150, 380, 500, 615,615, 730, 845},
    };
    
    public int getIndex(){
        return index;
    }

    public int getRoll(){
        return random.nextInt(3)+1;
    }

    public int getOldPos() {
        return oldPos;
    }

    public int getNewPos() {
        return newPos;
    }

    public int getIndex2(){
        return index2;
    }

    public int getScore() {
        return score;
    }

    public int getDiscSaved() {
        return discSaved;
    }

    
    //this is way more cleaner than before
    public void setPieces(int roll) {
        switch (roll) {
            case 1:
                index = path[0][3];
                break;
            case 2:
                index = path[0][2];
                break;
            case 3:
                index = path[0][1];
                break;
            case 4:
                index = path[0][0];
                break;
            case 5:
                index = path[1][0];
                break;
            case 6:
                index = path[1][1];
                break;
            case 7:
                index = path[1][2];
                break;
            case 8:
                index = path[1][3];
                break;
            case 9:
                index = path[1][4];
                break;
            case 10:
                index = path[1][5];
                break;
            case 11:
                index = path[1][6];
                break;
            case 12:
                index = path[1][7];
                break;
            case 13:
                index = path[1][8];
                break;
            case 14:
                index = path[0][8];
                break;
            case 15:
                index = path[0][7];
                break;
            default:
                break;
        }
    }


    public void setPieces2 (int roll) {
        switch (roll) {
            case 1:
                index2 = path[0][0];
                break;
            case 2:
                index2 = path[0][1];
                break;
            case 3:
                index2 = path[0][2];
                break;
            case 4:
                index2 = path[0][3];
                break;
            case 5:
                index2 = path[0][4];
                break;
            case 6:
                index2 = path[1][0];
                break;
            case 7:
                index2 = path[1][1];
                break;
            case 8:
                index2 = path[1][2];
                break;
            case 9:
                index2 = path[1][3];
                break;
            case 10:
                index2 = path[1][4];
                break;
            case 11:
                index2 = path[1][5];
                break;
            case 12:
                index2 = path[1][6];
                break;
            case 13:
                index2 = path[1][7];
                break;
            case 14:
                index2 = path[1][8];
                break;
            case 15:
                index2 = path[0][8];
                break;
            default:
                break;
        }
}

public int finish(int roll) {
    if (getIndex() == 730 && roll >= 14 ) {
        discSaved += 1;
        System.out.println("finished");
        score = score + 50;
        System.out.println("Your score is : " + score + " and you saved " + getDiscSaved());
        if (getOldPos()== getNewPos()) {
            score = score - 50;
        }
    }
    return 0;
}

}
