package allModels.game.gameFolder;

public class Board {
    private final int[][] player1Path = { { 4, 3, 2, 1, 8, 7 }, { 9, 10, 11, 12, 13, 14, 15, 16 } }; // this is the path
                                                                                                     // for player one
    private final int[][] player2Path = { { 20, 19, 18, 17 }, { 9, 10, 11, 12, 13, 14, 15, 16 } }; // this is the path
                                                                                                   // for player two
    private final int[][] rosettePositions = { { 1 }, { 13 } };
    private final int[][] rosettePositions2 = { { 17 }, { 13 } }; // this is the flowerPositions on the board
    private int row;
    private int column;
    private Dice d = new Dice();

    public void getPosition1() {
        int roll = d.getRoll();
        int sum = 0;
        sum = sum + roll;
        int index = -1;
        if (sum < 8) {
            for (int i = 0; i < player1Path.length; i++) {
                for (int j = 0; j < player1Path.length; j++) {
                    if (player1Path[i][sum] == player1Path[i][j]) {
                        row = player1Path[i][0];
                        index = player1Path[i][j];
                        column = index;
                    }
                }
            }
        } // end of the loop
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

}
