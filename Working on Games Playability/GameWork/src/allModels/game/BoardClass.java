package allModels.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BoardClass {
    private final List<PlayerClass> players;

    private final List<Integer> path1 = Arrays.asList(4, 3, 2, 1, 9, 10, 11, 12, 13, 14, 15, 16, 8, 7);
    private final List<Integer> path2 = Arrays.asList(20, 19, 18, 17, 9, 10, 11, 12, 13, 14, 15, 16, 24, 23);

    private final int[][] positions;

    public BoardClass(List<PlayerClass> players) {
        List<Rosette> rosettes = List.of(
                new Rosette(1),
                new Rosette(12),
                new Rosette(17));

        this.players = new ArrayList<>(players);
        for (PlayerClass player : players) {
            for (DiscsClass disc : player.getDiscs()) {
                disc.setDiscPosition(path1.get(0));
            }
        }
        positions = new int[StaticValues.ROWS][StaticValues.COLS];
        for (int row = 0; row < StaticValues.ROWS; row++) {
            for (int col = 0; col < StaticValues.COLS; col++) {
                if (row == 0) {
                    positions[row][col] = col + 1;
                }
                if (row == 1) {
                    positions[row][col] = col + 9;
                }
                if (row == 2) {
                    positions[row][col] = col + 16;
                }
            }
        }
    }

    public int getPos() {
        int index = 0;
        for (int row = 0; row < StaticValues.ROWS; row++) {
            for (int col = 0; col < StaticValues.COLS; col++) {
                boolean occupied = false;
                int playerPathPos = 0;
                for (PlayerClass player : players) {
                    if (player == players.get(0)) {
                        for (DiscsClass disc : players.get(0).getDiscs()) {
                            if (disc.getDiscPosition() != 0) {
                                playerPathPos = path1.get(disc.getDiscPosition() - 1);
                                if (playerPathPos == positions[row][col]) {
                                    occupied = true;
                                    index = playerPathPos;
                                }
                            }
                        }
                    }
                    if (player == players.get(1)) {
                        for (DiscsClass disc : players.get(1).getDiscs()) {
                            if (disc.getDiscPosition() != 0) {
                                playerPathPos = path2.get(disc.getDiscPosition() - 1);
                                if (playerPathPos == positions[row][col]) {
                                    occupied = true;
                                    index = playerPathPos;
                                }
                            }
                        }
                    }
                    if(occupied){
                        return -1;
                    }else{
                        return positions[row][col];
                    }
                }
            }
        }
        return index;
    }
}

