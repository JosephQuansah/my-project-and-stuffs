package allModels.game;

public class Rosette {
    private int rosettePosition;

    public Rosette(int rosettePosition){
        this.rosettePosition = rosettePosition;
    }
    public int getRosettePosition(){
        return rosettePosition;
    }

    public void setRosettePosition(int rosettePosition){
       this.rosettePosition = rosettePosition;
    }
}
