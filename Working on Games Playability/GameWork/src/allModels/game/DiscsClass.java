package allModels.game;

public class DiscsClass {
    private int discPosition;
    private final String layout;


    public DiscsClass(String layout, Color black){
        this.layout = layout;
    }

    public int getDiscPosition(){
        return discPosition;
    }

    public void setDiscPosition(int discPosition){
        this.discPosition = discPosition;
    }

    @Override
    public String toString(){
        return layout;
    }
}
