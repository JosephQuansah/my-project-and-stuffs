package allModels.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import allModels.userInformation.Player;

public class Game {

    private final List<PlayerClass> players;
    private final BoardClass board;

    //temp Player index
    private int playerIdx;

    //round Pawn indexes
    private int currentPlayer1DiscIdx;
    private int currentPlayer2DiscIdx;

    //in-game Players
    private final PlayerClass player1;
    private final PlayerClass player2;

    public Game() {
        String[] gamePlayers = new String[]{};
        DiscsClass[][] discs = new DiscsClass[gamePlayers.length][StaticValues.NUM_OF_DISCS];
        Color color;
        //Initialize the player's discs.
        for (int row = 0; row < discs.length; row++) {
            for (int col = 0; col < discs[row].length; col++) {
                if (row == 0) {
                    discs[row][col] = new DiscsClass("I", Color.BLACK);
                } else {
                    discs[row][col] = new DiscsClass("Z", Color.WHITE);
                }
            }
        }
        //Initialize the players.
        this.players = new ArrayList<PlayerClass>();
        for (int idx = 0; idx < gamePlayers.length; idx++) {
            players.add(new PlayerClass(gamePlayers[idx], discs[idx]));
        }

        for (PlayerClass player : players) {
            player.getDiscs();
        }
        this.board = new BoardClass(players);
        //Game players
        player1 = players.get(0);
        player2 = players.get(1);
        //round Pawn indexes
        currentPlayer1DiscIdx = 0;
        currentPlayer2DiscIdx = 0;

    }

    //check to see if  disc is movable
    public boolean moveDisc(DiscsClass disc, int value) {
    /*      todo: work in progress to update the discs positions based on the players paths */
        int position = disc.getDiscPosition();
        int newPosition = position + value;
        if (newPosition >= StaticValues.LENGTH_OF_PLAYERPATH) {
            // End run: if disc reaches end
            disc.setDiscPosition(0);
            return true;
        } else {
            disc.setDiscPosition(newPosition);
            return false;
        }
    }

    //Setting up the turn for next player
    public void setUpNextTurn() {
        this.playerIdx++;
        if (this.playerIdx == 2) {
            this.playerIdx = 0;
        }
    }

    //Checks the position of Round discs
    public boolean isPosOccupied(int currentDiscPos, int otherDiscPos) {
        if (currentDiscPos > 0 && otherDiscPos > StaticValues.START_OF_BATTLEGROUND) {
            return currentDiscPos == otherDiscPos;
        } else {
            return false;
        }
    }

    //checks isPosOccupied, if true, means the current player's move lead to eating the other player's disc
    public void isEaten(boolean isPosOccupied) {
        int roll;
        if (isPosOccupied) {
            System.out.println(getOtherPlayer().getName() + "'s Disc was sent back home");
            setOtherDiscPos(0);
            board.getPos();
            //re-set the turn for currentPlayer
            roll = getCurrentPlayer().takeTurn();
            this.moveDisc(getCurrentPlayerDisc(), roll);
        }
    }

    //when game finishes
    public boolean isGameOver(int currDiscIdx) {
        return currDiscIdx >= 7;
    }

    //Used to set the other Player's disc pos (mainly for isEaten method)
    public void setOtherDiscPos(int val) {
        getOtherPlayerDisc().setDiscPosition(val);
    }

    //Used to update the index of the playable Discs when saved
    public void setCurrentPlayer1DiscIdx(int currentPlayer1DiscIdx) {
        this.currentPlayer1DiscIdx = currentPlayer1DiscIdx;
    }

    //setting the current player 2 index
    public void setCurrentPlayer2DiscIdx(int currentPlayer2DiscIdx) {
        this.currentPlayer2DiscIdx = currentPlayer2DiscIdx;
    }

    //Getters
    //Get current player of a certain round, and the opponent(otherPlayer)
    public PlayerClass getCurrentPlayer() {
        //current round player
        return players.get(playerIdx);
    }

    //getting the other player
    public PlayerClass getOtherPlayer() {
        PlayerClass otherPlayer;
        if (playerIdx == 1) {
            otherPlayer = players.get(0);
        } else {
            otherPlayer = players.get(1);
        }
        return otherPlayer;
    }

    //Get the discs of the in-game Players: Used to check if whether their positions match or not
    public DiscsClass getCurrentPlayerDisc() {
        DiscsClass currDisc = null;
        if (getCurrentPlayer() == player1) {
            currDisc = player1.getDiscs()[this.currentPlayer1DiscIdx];
        } else if (getCurrentPlayer() == player2) {
            currDisc = player2.getDiscs()[this.currentPlayer1DiscIdx];
        }
        return currDisc;
    }

//getting the other player's disc
    public DiscsClass getOtherPlayerDisc() {
        DiscsClass otherDisc = null;
        if (getCurrentPlayer() == player1) {
            otherDisc = player2.getDiscs()[this.currentPlayer2DiscIdx];
        } else if (getCurrentPlayer() == player2) {
            otherDisc = player1.getDiscs()[this.currentPlayer1DiscIdx];
        }
        return otherDisc;
    }

    //getting player 1 
    public PlayerClass getPlayer1() {
        return player1;
    }

    //getting player 2
    public PlayerClass getPlayer2() {
        return player2;
    }

    //Get the index of Player's discs
    public int getCurrentPlayer1DiscIdx() {
        return currentPlayer1DiscIdx;
    }


    public int getCurrentPlayer2DiscIdx() {
        return currentPlayer2DiscIdx;
    }

}
/*
 * int seconds = 0; int minutes = 0; Timer timer = new Timer(); TimerTask task =
 * new TimerTask() {
 * 
 * @Override public void run() { seconds++; System.out.println(minutes + ":" +
 * seconds); } }; TimerTask task1 = new TimerTask() {
 * 
 * @Override public void run() { if (seconds == 10) { minutes++; seconds = 0; }
 * if (minutes == 10) { timer.cancel(); } } };
 * 
 * Timer gettingTime;
 * 
 * public int start() { timer.scheduleAtFixedRate(task, 1000, 1000);
 * timer.scheduleAtFixedRate(task1, 60, 10); gettingTime = timer; return
 * timer.purge(); }
 * 
 * public Timer getTimer() { return gettingTime; }
 */