package allModels.game;

public class StaticValues {
    public static final int ROWS = 3;
    public static final int COLS = 8;

    public static final int NUM_OF_DISCS = 7;
    public static final int DIE_FACES = 4;

    public static final int LENGTH_OF_PLAYERPATH = 14;
    public static final int START_OF_BATTLEGROUND = 380;
}
