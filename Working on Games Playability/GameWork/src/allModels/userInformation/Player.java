package allModels.userInformation;

import java.util.Random;

public class Player {
    private String name;
    private Random random = new Random();
    private int rolldice;
    private int id;

    public Player(){}

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int takeTurn() {
        rolldice = random.nextInt(4) + 1;
        return rolldice;
    }

    public int getId(){
        return id;
    }

}
