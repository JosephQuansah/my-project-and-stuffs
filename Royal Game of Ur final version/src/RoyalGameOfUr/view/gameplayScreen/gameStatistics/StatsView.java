package RoyalGameOfUr.view.gameplayScreen.gameStatistics;


import RoyalGameOfUr.model.Model;
import RoyalGameOfUr.model.StatisticsModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class StatsView extends VBox {
    private TableView<Model> table;
    private TableColumn<Model, String> nameColumn;
    private TableColumn<Model, String> secondPlayerColumn;
    private TableColumn<Model, Integer> scoreColumn;
    /*private TableColumn<Model, Integer> rolledDiceColumn;*/
    private final ObservableList<Model> stats = FXCollections.observableArrayList();

    public StatsView() {
        InitializeNodes();
        layoutNodes();
    }

    private void InitializeNodes() {
        table = new TableView<>();
        nameColumn = new TableColumn<>("Player Name");
        secondPlayerColumn = new TableColumn<>("Opponent");
        scoreColumn = new TableColumn<>("Score");
        /*rolledDiceColumn = new TableColumn<>("Rolled");*/
    }


    private void layoutNodes() {
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("player1Name"));
        nameColumn.setMinWidth(200);
        secondPlayerColumn.setCellValueFactory(new PropertyValueFactory<>("player2Name"));
        secondPlayerColumn.setMinWidth(200);
        scoreColumn.setCellValueFactory(new PropertyValueFactory<>("score"));
        scoreColumn.setMinWidth(100);
      /*  rolledDiceColumn.setCellValueFactory(new PropertyValueFactory<>("randomNumber"));
        rolledDiceColumn.setMinWidth(100);*/
        getChildren().addAll(table);
        table.setItems(getTable());
        table.getColumns().addAll(nameColumn, scoreColumn,  secondPlayerColumn/*,rolledDiceColumn*/);
    }

    public ObservableList<Model> getTable() {
        return stats;
    }

    public TableView<Model> getTableNames() {
        return table;
    }
}
