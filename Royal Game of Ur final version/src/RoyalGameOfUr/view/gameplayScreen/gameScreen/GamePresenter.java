package RoyalGameOfUr.view.gameplayScreen.gameScreen;

import RoyalGameOfUr.model.Model;
import RoyalGameOfUr.model.StatisticsModel;
import RoyalGameOfUr.view.gameplayScreen.gameStatistics.StatsView;
import RoyalGameOfUr.view.startScreen.StartScreenPresenter;
import RoyalGameOfUr.view.startScreen.StartScreenView;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.util.Random;

public class GamePresenter {
    private final GameView view;
    private final Model model;
    private final StatsView statsView = new StatsView();
    private int sum = 0;
    private int sum2 = 0;
    private int sum3 = 0;
    private int unknownNumber = 0;
    private final Random r = new Random();


    public GamePresenter(GameView view, Model model) {
        this.view = view;
        this.model = model;
        addEventsHandlers();
        updateViews();
    }

    public void translate2() {
        if (sum2 < 16) {
            model.setRow2(sum2);
            if (view.getColumn2() == 695 && model.getPlayer2Roll() > 3 || view.getColumn2() == 605 && model.getPlayer2Roll() > 3) {
                sum2 = 0;
                view.setPieces2(sum2);
            } else {
                if (sum2 < 5 || sum2 > 13) {
                    view.Piece2(model.getRow2(), view.getColumn2());
                } else {
                    view.Piece2(model.getRow2(), view.getColumn2());
                }
            }
            model.finish2(sum2);
        } else {
            sum2 = 0;
            view.setPieces2(sum2);
        }
    }


    public void translate() {
        /*start by saying that if the number rolled is 4 then the player starts on the board at position 1 else can't start*/
        if (sum > 3) {
            view.setPieces(0);
            if (sum < 16) {
                model.setRow(sum);
                if (view.getColumn1() == 695 && model.getPlayer1Roll() > 3 || view.getColumn1() == 605 && model.getPlayer1Roll() > 3) {
                    sum = 0;
                    view.setPieces2(sum);
                } else {
                    if (sum < 5 || sum > 13) {
                        view.Piece1(model.getRow(), view.getColumn1());
                    } else {
                        view.Piece1(model.getRow(), view.getColumn1());
                    }
                }
                model.finish(sum);

            } else {
                sum = 0;
                view.setPieces2(sum);
            }
        }
    }

    public void translate3() {
        if (sum3 > 3) {
            view.setPieces2(0);
            if (sum3 < 16) {
                model.setRow2(sum3);
                if (view.getColumn2() == 695 && unknownNumber > 3 || view.getColumn2() == 605 && unknownNumber > 3) {
                    sum3 = 0;
                    view.setPieces2(sum3);
                } else {
                    if (sum3 < 5 || sum3 > 13) {
                        view.Piece2(model.getRow2(), view.getColumn2());
                    } else {
                        view.Piece2(model.getRow2(), view.getColumn2());
                    }
                }
                model.finish2(sum3);
            } else {
                sum3 = 0;
                view.setPieces2(sum3);
            }
        }
    }


    private void addEventsHandlers() {
        view.getImage1().setOnMouseClicked(e -> {
            view.drawBackground();
            view.getImage1().setImage(new Image("/images/dice/dice" + model.getPlayer1Roll() + ".png"));
            view.setPieces(sum = sum + model.getPlayer1Roll());
            translate();
            unknownNumber = r.nextInt(3) + 1;
            translate3();
            view.setPieces2(sum3 += unknownNumber);
            if (view.getColumn1() == view.getColumn2() && model.getRow() == 255) {
                sum3 = 0;
                view.setPieces2(sum3);
            }
            model.scorePlayer();
            statsView.getTable().add(new Model(model.getPlayer1Name(), model.getPlayer2Name(), model.getScore()));
            if (model.getDiscSaved() > 6) {
                gameOver();
            }

        });

        view.getImage2().setOnMouseClicked(e -> {
            view.drawBackground();
            view.getImage2().setImage(new Image("/images/dice/Bdice" + model.getPlayer2Roll() + ".png"));
//            view.setPieces2(sum2 = sum2 + model.getPlayer2Roll());

            view.setPieces2(sum3 += unknownNumber);
            translate();
            translate3();
            if (view.getColumn2() == view.getColumn1() && model.getRow2() == 255) {
                sum = 0;
                view.setPieces(sum);
            }
            model.setScore2();
            statsView.getTable().add(new Model(model.getPlayer2Name(), model.getPlayer1Name(), model.getScore()));
            if (model.getDiscSaved() > 6) {
                gameOver();
            }
        });

        view.getButton1().setOnAction(e -> {
            SetUpStartScreen();
        });

        view.getTimer().setText("10:00");
        updateViews();

    }


    private void updateViews() {
        view.drawBackground();
        view.getImage1().setImage(new Image("/images/dice/dice" + model.getPlayer1Roll() + ".png"));
        view.setPieces(sum = sum + model.getPlayer1Roll());
        translate();
        view.getImage2().setImage(new Image("/images/dice/Bdice" + model.getPlayer2Roll() + ".png"));
//        view.setPieces2(sum2 = sum2 + model.getPlayer2Roll());
//        translate2();
        unknownNumber = r.nextInt(3) + 1;
        view.setPieces2(sum3 += unknownNumber);
        translate3();
        model.finish2(sum3);
        model.finish(sum);
//        model.finish2(sum2);
        statsView.getTable().add(new Model(model.getPlayer2Name(), model.getPlayer1Name(), model.getScore()));
    }

    private void SetUpStartScreen() {
        StartScreenView startScreenView = new StartScreenView();
        new StartScreenPresenter(model, startScreenView);
        view.getScene().setRoot(startScreenView);
        Stage rgouStage = (Stage) startScreenView.getScene().getWindow();
        rgouStage.setMaxWidth(650);
        rgouStage.setMaxHeight(600);
    }


    private void stats() {
        Stage stage = new Stage();
        stage.setTitle("Game Statistics");
        stage.setScene(new Scene(statsView));
        stage.show();
    }

    private void gameOver() {
        StatisticsModel d = new StatisticsModel(model.getPlayer1Name(), model.getScore(), model.getScore2(), model.getPlayer1Roll(), model.getDiscSaved(), model.getPlayer2Name());
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Game OVER");
        alert.setContentText("Do you really want to display stats?");
        alert.setTitle("Warning");
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/images/icon.png"));
        alert.getButtonTypes().clear();
        ButtonType no = new ButtonType("NO");
        ButtonType yes = new ButtonType("YES");
        alert.getButtonTypes().addAll(no, yes);
        alert.showAndWait();
        if (alert.getResult() == null || alert.getResult().equals(no)) {
            Platform.exit();
        } else {
            stats();
            d.dataBase();
        }
    }
}

/*Notes: Left with how to make the database work well*/
/*Also adding highlighters of discs to show which disc is to be moved*/



