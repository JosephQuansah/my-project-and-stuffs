package RoyalGameOfUr.view.gameplayScreen.gameScreen;

import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

public class GameView extends BorderPane {
    private Button button1;
    private Canvas canvas;
    private GraphicsContext gc;
    private GraphicsContext gc2;
    private Label text1;
    private Label text2;
    private Label timer;
    private Image image;
    private ImageView imageView;
    private ImageView diceImage1;
    private ImageView diceImage2;
    private HBox box1;
    private HBox box2;
    private int column1 = 0;
    private int column2 = 0;

    private final int[][] path = new int[][]{
            {300, 210, 115, 20, 405, 495, 605, 695},
            {20, 115, 210, 300, 405, 495, 605, 695},
    };


    public GameView() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        button1 = new Button("Menu");
        canvas = new Canvas(1000, 600);
        gc = canvas.getGraphicsContext2D();
        gc2 = canvas.getGraphicsContext2D();
        image = new Image("/images/dice/board.jpg");
        imageView = new ImageView(image);
        diceImage1 = new ImageView();
        diceImage2 = new ImageView();
        box1 = new HBox();
        box2 = new HBox();
        text1 = new Label();
        text2 = new Label();
        timer = new Label();
    }

    private void layoutNodes() {
        imageView.setFitHeight(300);
        imageView.setFitWidth(850);
        setCenter(canvas);
        box1.getChildren().addAll(button1, text1, diceImage1);
        setTop(box1);
        box2.getChildren().addAll(timer,text2,diceImage2);
        setBottom(box2);
        box1.setSpacing(500);
        box2.setSpacing(500);
        setMargin(box1, new Insets(10, 50, 10, 50));
        setMargin(box2, new Insets(10, 50, 10, 50));
        text1.setFont(new Font(30));
        text2.setFont(new Font(30));
        timer.setFont(new Font(30));
        diceImage1.setFitHeight(80);
        diceImage1.setFitWidth(80);
        diceImage2.setFitHeight(80);
        diceImage2.setFitWidth(80);
    }

    void Piece1(int i, int j) { //adding the color adjustments to the discs
        gc.setFill(Paint.valueOf("RED"));
        gc.fillOval(j, i, 65, 65);
    }

    void Piece2(int i, int j) { //adding the color adjustments to the discs
        gc.setFill(Paint.valueOf("BLUE"));
        gc.fillOval(j, i, 65, 65);
    }
    public void setPieces(int roll) {
        switch (roll) {
            case 1:
                column1 = path[0][0];
                break;
            case 2:
                column1 = path[0][1];
                break;
            case 3:
                column1 = path[0][2];
                break;
            case 4:
                column1 = path[0][3];
                break;
            case 5:
                column1 = path[1][0];
                break;
            case 6:
                column1 = path[1][1];
                break;
            case 7:
                column1 = path[1][2];
                break;
            case 8:
                column1 = path[1][3];
                break;
            case 9:
                column1 = path[1][4];
                break;
            case 10:
                column1 = path[1][5];
                break;
            case 11:
                column1 = path[1][6];
                break;
            case 12:
                column1 = path[1][7];
                break;
            case 13:
                column1 = path[0][7];
                break;
            case 14:
                column1 = path[0][6];
                break;
            default:
                break;
        }

    }
    public void setPieces2(int roll) {
        switch (roll) {
            case 1:
                column2 = path[0][0];
                break;
            case 2:
                column2 = path[0][1];
                break;
            case 3:
                column2 = path[0][2];
                break;
            case 4:
                column2 = path[0][3];
                break;
            case 5:
                column2 = path[1][0];
                break;
            case 6:
                column2 = path[1][1];
                break;
            case 7:
                column2 = path[1][2];
                break;
            case 8:
                column2 = path[1][3];
                break;
            case 9:
                column2 = path[1][4];
                break;
            case 10:
                column2 = path[1][5];
                break;
            case 11:
                column2 = path[1][6];
                break;
            case 12:
                column2 = path[1][7];
                break;
            case 13:
                column2 = path[0][7];
                break;
            case 14:
                column2 = path[0][6];
                break;
            default:
                break;
        }
    }

    void drawBackground() {
        gc2.drawImage(image, 0, 150, 780, 275);
    }

    ImageView getImage1() {
        return diceImage1;
    }

    ImageView getImage2() {
        return diceImage2;
    }

    Button getButton1() {
        return button1;
    }
    public int getColumn1() {
        return column1;
    }
    public int getColumn2() {
        return column2;
    }
    public Label getText1() {
        return text1;
    }

    public Label getText2() {
        return text2;
    }

    Label getTimer() {
        return timer;
    }
}
