package RoyalGameOfUr.view.gameplayScreen.otherScreen;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public class Player2Info extends GridPane {
    private Button playGame;
    private Label label;
    private TextField field;
    private HBox box;
    private Label chooseColor;
    private HBox choiceBox;
    private ToggleGroup group;
    private RadioButton choice;
    private RadioButton choice2;

    public Player2Info() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        playGame = new Button("Play Game");
        label = new Label("Player2 enter your name: ");
        field = new TextField();
        box = new HBox();
        chooseColor = new Label("Please choose a color for your disc");
        group = new ToggleGroup();
        choice = new RadioButton("red");
        choice2 = new RadioButton("blue");

        choiceBox = new HBox();
    }

    private void layoutNodes() {
        label.setFont(new Font(15));
        box.getChildren().addAll(label, field);
        box.setSpacing(20);
        setHalignment(box, HPos.CENTER);
        setHgap(20);
        addRow(0, box);
        setPadding(new Insets(20));
        addRow(2, playGame);
        addRow(4, chooseColor);
        addRow(6, choiceBox);
        choiceBox.getChildren().addAll(choice, choice2);
        choiceBox.setSpacing(15);
        setVgap(5);
        choice.setToggleGroup(group);
        choice.setSelected(true);
        choice2.setToggleGroup(group);
    }

    public TextField getField() {
        return field;
    }

    public Button getPlayGame() {
        return playGame;
    }

    public RadioButton getChoice(){
        return choice;
    }

    public RadioButton getChoice2(){
        return choice2;
    }

}
