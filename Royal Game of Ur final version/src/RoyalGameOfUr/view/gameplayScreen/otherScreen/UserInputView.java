package RoyalGameOfUr.view.gameplayScreen.otherScreen;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

    public class UserInputView extends BorderPane {
        private Button playerButton;
        private Button player2Button;
        private HBox horizontal;
        private HBox horizontal2;
        private Label label;
        private VBox vertical;
        private TextField field;
        private Label chooseColor;
        private RadioButton choice;
        private RadioButton choice2;
        private HBox choiceBox;

        public UserInputView() {
            initializeNodes();
            layoutNodes();
        }

        private void initializeNodes() {

            horizontal = new HBox();
            horizontal2 = new HBox();
            playerButton = new Button("One Player");
            player2Button = new Button("Two Players");
            label = new Label("Player1 enter your name: ");
            vertical = new VBox();
            field = new TextField();
            chooseColor = new Label("Please choose a color for your disc");
            choice = new RadioButton("Blue");
            choice2 = new RadioButton("Red");
            choiceBox = new HBox();

        }

        private void layoutNodes() {
            playerButton.setPrefHeight(50);
            player2Button.setPrefHeight(50);
            horizontal.getChildren().addAll(label, field);
            horizontal2.getChildren().addAll(playerButton, player2Button);
            horizontal.setSpacing(20);
            horizontal2.setSpacing(50);
            vertical.getChildren().addAll(horizontal, horizontal2, chooseColor, choiceBox);
            setCenter(vertical);
            setPadding(new Insets(20));
            vertical.setSpacing(10);
            field.setMaxWidth(450);
            label.setFont(new Font(17));
            choiceBox.getChildren().addAll(choice, choice2);
            choiceBox.setSpacing(20);
        }


        public TextField getField() {
            return field;
        }

        public Button getPlayerButton() {
            return playerButton;
        }

        public Button getPlayer2Button() {
            return player2Button;
        }

        public RadioButton getChoice(){
            return choice;
        }

        public RadioButton getChoice2(){
            return choice2;
        }
    }

