package RoyalGameOfUr.view.gameplayScreen.otherScreen;

import RoyalGameOfUr.model.Model;
import RoyalGameOfUr.model.StatisticsModel;
import RoyalGameOfUr.view.gameplayScreen.gameScreen.GamePresenter;
import RoyalGameOfUr.view.gameplayScreen.gameScreen.GameView;
import RoyalGameOfUr.view.gameplayScreen.gameStatistics.StatsView;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Presenter {
    private final UserInputView view;
    private final Model model;
    private final Player2Info view2 = new Player2Info();
    private final GameView gameView = new GameView();
    private final StatsView statsView = new StatsView();

    public Presenter(UserInputView view, Model model) {
        this.view = view;
        this.model = model;
        addEventsHandlers();
        updateView();
    }

    private void addEventsHandlers() {

        view.getPlayer2Button().setOnAction(e -> newScene());

        view.getPlayerButton().setOnAction(e -> {
            String name1 = view.getField().getText();
            model.setPlayer1Name(name1);
            gameView.getText1().setText(name1);
            String name2 = "CPU";
            model.scorePlayer();
            statsView.getTable().add(new Model(model.getPlayer1Name(), model.getPlayer2Name(), model.getScore()));
            if (view.getField().equals(" ")) {
                try {
                    throw new Exception("Provide your name");
                } catch (Exception exception) {
                    System.err.println("Provide your name");
                }
            } else {
                model.setPlayer2Name(name2);
                gameView.getText2().setText(name2);
                gameScene();
            }
            updateView();
        });

        view2.getPlayGame().setOnAction(e -> {
            String name1 = view.getField().getText();
            model.setPlayer1Name(name1);
            gameView.getText1().setText(name1);
            String name2 = view2.getField().getText();
            model.setPlayer2Name(name2);
            model.scorePlayer();
            statsView.getTable().add(new Model(model.getPlayer1Name(), model.getPlayer2Name(), model.getScore()));
            if (view.getField().equals(" ")) {
                try {
                    throw new Exception("Provide your name");
                } catch (Exception exception) {
                    System.err.println("Provide your name");
                }
            } else {
                gameView.getText2().setText(name2);
                gameScene2();
            }
            updateView();
        });
    }

    private void updateView() {
        String name2 = view2.getField().getText();
        model.setPlayer2Name(name2);
        String name1 = view.getField().getText();
        model.setPlayer1Name(name1);
        view2.getField().getText();
        statsView.getTable().add(new Model(model.getPlayer1Name(), model.getPlayer2Name(), model.getScore()));
    }

    void newScene() {
        view.getScene().setRoot(view2);
    }

    void gameScene2() {
        new GamePresenter(gameView, new Model(model.getPlayer1Name(), model.getPlayer2Name(), model.getScore()));
        view2.getScene().setRoot(gameView);
        gameView.getScene().getWindow().sizeToScene();
        gameView.getScene().getWindow().centerOnScreen();
    }

    void gameScene() {
        new GamePresenter(gameView, new Model(model.getPlayer1Name(), model.getPlayer2Name(), model.getScore()));
        view.getScene().setRoot(gameView);
        gameView.getScene().getWindow().sizeToScene();
        gameView.getScene().getWindow().centerOnScreen();
    }


}
