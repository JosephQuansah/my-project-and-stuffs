package RoyalGameOfUr.view.startScreen;

import RoyalGameOfUr.model.Model;
import RoyalGameOfUr.model.game.Die;
import RoyalGameOfUr.view.creditsScreen.CreditsPresenter;
import RoyalGameOfUr.view.creditsScreen.CreditsView;
import RoyalGameOfUr.view.gameplayScreen.otherScreen.Presenter;
import RoyalGameOfUr.view.gameplayScreen.otherScreen.UserInputView;
import RoyalGameOfUr.view.instructionsScreen.InstructionsPresenter;
import RoyalGameOfUr.view.instructionsScreen.InstructionsView;
import RoyalGameOfUr.view.settingsScreen.SettingsPresenter;
import RoyalGameOfUr.view.settingsScreen.SettingsView;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class StartScreenPresenter {
    private final Model  model;
    private final StartScreenView view;


    public StartScreenPresenter(Model  model, StartScreenView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
    }

    private void addEventHandlers() {
        view.getStartGame().setOnAction(event -> setGameView());
        view.getInstructions().setOnAction(event -> setInstructionsView());
        view.getExit().setOnAction(event -> Platform.exit());
        view.getSettingsPage().setOnAction(event -> setSettingsView());
        view.getCredits().setOnAction(event -> setCreditsView());
        view.getExit().setOnAction(this::closeApplication);
    }

    private void setGameView() {
        UserInputView userInputView = new UserInputView();
        Model model = new Model("Aby","Danny",50);
        new Presenter(userInputView, model);
        view.getScene().setRoot(userInputView);
    }

    private void setInstructionsView() {
        InstructionsView instructionsView = new InstructionsView();
        new InstructionsPresenter(model, instructionsView);
        view.getScene().setRoot(instructionsView);
    }

    private void setCreditsView() {
        CreditsView creditsView = new CreditsView();
        new CreditsPresenter(model, creditsView);
        view.getScene().setRoot(creditsView);
    }

    private void setSettingsView() {
        SettingsView settingsView = new SettingsView();
        new SettingsPresenter(model, settingsView);
        view.getScene().setRoot(settingsView);
    }

    public void addWindowEventHandlers() {
        view.getScene().getWindow().setOnCloseRequest(this::closeApplication);
    }

    private void closeApplication(Event event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("You are about to quit the game.");
        alert.setContentText("Do you really want to leave?");
        alert.setTitle("Warning");
        alert.setGraphic(new ImageView(new Image("/images/sadface.png")));
        //Create/set alert dialog pop-up as a stage to set an icon for it
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/images/icon.png"));
        alert.getButtonTypes().clear();
        ButtonType no = new ButtonType("NO");
        ButtonType yes = new ButtonType("YES");
        alert.getButtonTypes().addAll(no, yes);
        alert.showAndWait();
        if (alert.getResult() == null || alert.getResult().equals(no)) {
            event.consume();
        } else {
            Platform.exit();
        }
    }

    private void updateMuteView(double vol) {
        if (vol == 0) {
            view.getMute().setGraphic(new ImageView(new Image("/images/mute.png")));
        } else {
            view.getMute().setGraphic(new ImageView(new Image("/images/unmute.png")));
        }
    }

}
