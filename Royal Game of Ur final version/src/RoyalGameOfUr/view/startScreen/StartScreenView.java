package RoyalGameOfUr.view.startScreen;

import RoyalGameOfUr.view.button.MuteButton;
import RoyalGameOfUr.view.button.MenuButton;
import RoyalGameOfUr.view.setting.display.Effects;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class StartScreenView extends BorderPane {
    private MenuButton startGame;
    private MenuButton settingsPage;
    private MenuButton instructions;
    private MenuButton credits;
    private MenuButton exit;
    private MuteButton mute;
    private ImageView banner;
    private final Image topImage = new Image("/images/rgouv5.png");


    public StartScreenView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        startGame = new MenuButton("Start the game");
        settingsPage = new MenuButton("Settings");
        instructions = new MenuButton("Instructions");
        credits = new MenuButton("Credits");
        exit = new MenuButton("Exit");
        mute = new MuteButton();
        this.banner = new ImageView(topImage);
    }

    private void layoutNodes() {
        this.setEffect(Effects.colorAdjust);
        setTop(addHbox());
        setCenter(addGpane());
        setStyle("-fx-background-image: url('/images/backgroundv4.jpg')");
        setPrefSize(500, 600);
    }

    private GridPane addGpane() {
        GridPane gp = new GridPane();
        gp.getChildren().addAll(startGame, settingsPage, instructions, credits, exit,
                banner);
        gp.setVgap(10);
        gp.setAlignment(Pos.CENTER);
        //Constraints for nodes
        GridPane.setConstraints(banner, 0, 0, 2, 1);
        GridPane.setConstraints(startGame, 0, 1, 2, 1, HPos.CENTER, VPos.TOP);
        GridPane.setConstraints(settingsPage, 0, 2, 2, 1, HPos.CENTER, VPos.TOP);
        GridPane.setConstraints(instructions, 0, 3, 2, 1, HPos.CENTER, VPos.TOP);
        GridPane.setConstraints(credits, 0, 4, 2, 1, HPos.CENTER, VPos.TOP);
        GridPane.setConstraints(exit, 0, 5, 2, 1, HPos.CENTER, VPos.TOP);
        setMargin(banner, new Insets(0, 0, 50, 0));
        return gp;
    }

    private HBox addHbox() {
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.TOP_RIGHT);
        hbox.getChildren().add(mute);
        return hbox;
    }

    MenuButton getStartGame() {
        return startGame;
    }

    MenuButton getSettingsPage() {
        return settingsPage;
    }

    MenuButton getInstructions() {
        return instructions;
    }

    MenuButton getCredits() {
        return credits;
    }

    MenuButton getExit() {
        return exit;
    }

    MuteButton getMute() {
        return mute;
    }

}
