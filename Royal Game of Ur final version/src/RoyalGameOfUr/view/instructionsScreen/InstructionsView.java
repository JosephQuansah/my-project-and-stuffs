package RoyalGameOfUr.view.instructionsScreen;

import RoyalGameOfUr.view.Styles;
import RoyalGameOfUr.view.button.MuteButton;
import RoyalGameOfUr.view.button.ReturnButton;
import RoyalGameOfUr.view.button.SettingsButton;
import RoyalGameOfUr.view.setting.display.Effects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

public class InstructionsView extends BorderPane {
    private ReturnButton returnButton;
    private MuteButton mute;
    private SettingsButton gear;
    private Text instructions;
    private ImageView bannerView;
    private final Image image = new Image("/images/instructionsbanner.png");


    public InstructionsView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        returnButton = new ReturnButton();
        mute = new MuteButton();
        gear = new SettingsButton();
        instructions = new Text();
        bannerView = new ImageView(image);
    }

    private void layoutNodes() {
        this.setEffect(Effects.colorAdjust);
        this.setTop(addHBox());
        this.setCenter(addGPane());
        this.setPrefSize(500, 600);
        this.setStyle("-fx-background-image: url('/images/backgroundv4.jpg')");
    }


    private GridPane addGPane() {
        instructions.setText("It is generally agreed that the Royal game of Ur is a race game- the aim is \n" +
                "to get all 7 pieces around the board to the finish point first.\n" +
                "\n 1. Players agree on who plays the first turn\n" +
                " 2. Players take turns to throw 2 binary dice and move one of their pieces.\n" +
                " 3. Only one piece may be moved, from 0 to 4 positions per dice throw.\n" +
                " 4. Pieces must always move forward following the track\n" +
                " 5. if a counter lands upon a square occupied by the opponent,\n" +
                "   the landed upon is sent off the board and must start again");
        instructions.setStyle(Styles.INSTRUCTIONS_STYLE);
        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setAlignment(Pos.CENTER);
        grid.add(bannerView, 0, 1);
        grid.add(instructions, 0, 2);
        GridPane.setMargin(bannerView, new Insets(80, 50, 40, 30));
        GridPane.setMargin(instructions, new Insets(0, 0, 70, 20));
        return grid;
    }
    private HBox addHBox() {
        HBox hbox = new HBox();
        HBox hbox2 = new HBox();
        hbox.setSpacing(522);
        hbox2.setSpacing(0);
        hbox2.setAlignment(Pos.TOP_RIGHT);
        hbox2.getChildren().addAll(gear, mute);
        hbox.getChildren().addAll(returnButton, hbox2);
        return hbox;
    }

    Button getGear() { return gear; }
    Button getReturnButton() {
        return returnButton;
    }
}
