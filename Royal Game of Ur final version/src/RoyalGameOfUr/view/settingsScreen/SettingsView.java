package RoyalGameOfUr.view.settingsScreen;

import RoyalGameOfUr.view.Styles;
import RoyalGameOfUr.view.setting.audio.SoundTrack;
import RoyalGameOfUr.view.button.ReturnButton;
import RoyalGameOfUr.view.setting.display.Effects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class SettingsView extends BorderPane {
    private Slider brightness;
    private Slider contrast;
    private Slider music;
    private Label brightnessLabel;
    private Label contrastLabel;
    private Label musicLabel;
    private ReturnButton returnButton;
    private ImageView banner;
    private final Image topImage = new Image("/images/settings.png");

    public SettingsView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        music = new Slider(0.0, 1, SoundTrack.getVolume());
        brightness = new Slider(0.0, 1, Effects.getBrightness());
        contrast = new Slider(0.0, 1, Effects.getContrast());
        musicLabel = new Label("Music");
        brightnessLabel = new Label("Brightness");
        contrastLabel = new Label("Contrast");
        returnButton = new ReturnButton();
        banner = new ImageView(topImage);
    }

    private void layoutNodes() {
        this.setEffect(Effects.colorAdjust);
        setTop(addHbox());
        setCenter(addGpane());
        brightnessLabel.setStyle(Styles.SETTINGS_STYLE);
        contrastLabel.setStyle(Styles.SETTINGS_STYLE);
        musicLabel.setStyle(Styles.SETTINGS_STYLE);
        setStyle("-fx-background-image: url('/images/backgroundv4.jpg')");
        setPrefSize(500, 600);
    }

    private GridPane addGpane() {
        GridPane gp = new GridPane();
        gp.getChildren().addAll(music, brightness, contrast, brightnessLabel, contrastLabel, musicLabel, banner);
        gp.setVgap(20);
        gp.setAlignment(Pos.CENTER);
        //Constraints for nodes
        music.setMaxWidth(250);
        brightness.setMaxWidth(250);
        contrast.setMaxWidth(250);
        GridPane.setMargin(contrastLabel, new Insets(0,0,0,130));
        GridPane.setMargin(contrast, new Insets(0,0,0,20));
        GridPane.setMargin(musicLabel, new Insets(0,0,0,130));
        GridPane.setMargin(music, new Insets(0,0,0,20));
        GridPane.setMargin(brightnessLabel, new Insets(0,0,0,130));
        GridPane.setMargin(brightness, new Insets(0,0,0,20));
        GridPane.setMargin(banner, new Insets(0, 0, 50, 0));
        GridPane.setConstraints(banner, 0, 0, 2, 1);
        GridPane.setConstraints(brightness, 1, 1, 1, 1);
        GridPane.setConstraints(brightnessLabel, 0, 1, 1, 1);
        GridPane.setConstraints(contrast, 1, 2, 1, 1);
        GridPane.setConstraints(contrastLabel, 0, 2, 1, 1);
        GridPane.setConstraints(music, 1, 3, 1, 1);
        GridPane.setConstraints(musicLabel, 0, 3, 1, 1);
        return gp;
    }

    private HBox addHbox() {
        HBox hBox = new HBox();
        hBox.getChildren().add(returnButton);
        hBox.setAlignment(Pos.TOP_LEFT);
        return hBox;
    }

    Slider getMusicSlider() {
        return music;
    }

    Slider getBrightnessSlider() {
        return brightness;
    }

    Slider getBloomSlider() {
        return contrast;
    }

    ReturnButton getReturnButton() {
        return returnButton;
    }

}
