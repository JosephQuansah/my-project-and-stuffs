package RoyalGameOfUr.view.settingsScreen;

import RoyalGameOfUr.model.Model;
import RoyalGameOfUr.model.game.Die;
import RoyalGameOfUr.view.setting.audio.SoundTrack;
import RoyalGameOfUr.view.setting.display.Effects;
import RoyalGameOfUr.view.startScreen.StartScreenPresenter;
import RoyalGameOfUr.view.startScreen.StartScreenView;

public class SettingsPresenter {
    private final Model model;
    private final SettingsView view;

    public SettingsPresenter(Model  model, SettingsView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
    }


    private void addEventHandlers() {
        view.getMusicSlider().setOnMouseDragged(event -> {
            double vol = view.getMusicSlider().getValue();
            SoundTrack.adjustVol(vol);
        });
        view.getBrightnessSlider().setOnMouseDragged(event -> {
            double val = view.getBrightnessSlider().getValue();
            Effects.adjustBrightness(val);
        });
        view.getBloomSlider().setOnMouseDragged(event -> {
            double val = view.getBloomSlider().getValue();
            Effects.adjustContrast(val);
        });
        view.getReturnButton().setOnAction(event -> {
            setStartMenuView();
        });
    }

    private void setStartMenuView() {
        StartScreenView startView = new StartScreenView();
        StartScreenPresenter presenter = new StartScreenPresenter(model, startView);
        view.getScene().setRoot(startView);
    }

    private void updateView() {
    }
}
