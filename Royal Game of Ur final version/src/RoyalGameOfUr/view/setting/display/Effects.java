package RoyalGameOfUr.view.setting.display;

import javafx.scene.effect.ColorAdjust;

public class Effects {
    public static final ColorAdjust colorAdjust = new ColorAdjust();

    public static void adjustBrightness(double val) {
        colorAdjust.setBrightness(val);
    }

    public static double getBrightness() { return colorAdjust.getBrightness(); }

    public static void adjustContrast(double val) {
        colorAdjust.setContrast(val);
    }

    public static double getContrast() {
        return colorAdjust.getContrast();
    }
}
