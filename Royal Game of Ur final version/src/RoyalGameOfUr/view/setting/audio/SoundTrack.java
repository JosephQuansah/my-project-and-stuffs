package RoyalGameOfUr.view.setting.audio;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/** (La Valse d'Amélie); Written by (Yann Tiersen);
 * Performed by (Yann Tiersen); Courtesy of (℗ 2001 Victoires Productions Under Exclusive Licence to Parlophone /
 * Warner Music France, a Warner Music Group Company).
 */

public class SoundTrack {
    private static final Media BACKGROUND_TRACK = new Media(SoundTrack.class.getClassLoader().getResource("audio/backgrounding.mp3").toExternalForm());
    private static final MediaPlayer player = new MediaPlayer(BACKGROUND_TRACK);

    public static void playBackGroundMusic() {
        player.setVolume(0.15);
        player.setAutoPlay(true);
        player.setCycleCount(MediaPlayer.INDEFINITE);
    }

    public static void muteTrack() {
        player.setMute(true);
    }

    public static void unmuteTrack() {
        player.setMute(false);
    }

    public static boolean isMuted() {
        return player.isMute();
    }
    public static void adjustVol(double vol) {
        player.setVolume(vol);
    }

    public static double getVolume() {
        return player.getVolume();
    }
}