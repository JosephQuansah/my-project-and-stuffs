package RoyalGameOfUr.view;

public class Styles {
    public final static String INSTRUCTIONS_STYLE = "-fx-fill:wheat; -fx-font-family: 'Comic Sans MS'; -fx-font-size: 1.3em;";
    public final static String SETTINGS_STYLE = "-fx-text-fill:white; -fx-border-color: none; -fx-background-color: none; -fx-font-family: 'Comic Sans MS';-fx-font-size: 1.3em";
    public final static String CREDITS_STYLE = "-fx-fill:wheat; -fx-font-family: 'Comic Sans MS'; -fx-font-size: 1.3em;-fx-text-alignment: center;";
}
