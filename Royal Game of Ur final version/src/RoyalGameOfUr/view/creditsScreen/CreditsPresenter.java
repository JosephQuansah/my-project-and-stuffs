package RoyalGameOfUr.view.creditsScreen;

import RoyalGameOfUr.model.Model;
import RoyalGameOfUr.model.game.Die;
import RoyalGameOfUr.view.settingsScreen.SettingsPresenter;
import RoyalGameOfUr.view.settingsScreen.SettingsView;
import RoyalGameOfUr.view.startScreen.StartScreenPresenter;
import RoyalGameOfUr.view.startScreen.StartScreenView;

public class CreditsPresenter {
    private final CreditsView view;
    private final Model model;

    public CreditsPresenter(Model  model, CreditsView view) {
        this.view = view;
        this.model = model;
        addEventHandlers();
    }

    private void addEventHandlers() {
        view.getReturnButton().setOnAction(event -> {
            setStartMenuView();
        });
        view.getGear().setOnAction(event -> {
            setSettingsView();
        });
    }

    private void setStartMenuView() {
        StartScreenView startView = new StartScreenView();
        StartScreenPresenter presenter = new StartScreenPresenter(model, startView);
        view.getScene().setRoot(startView);
    }

    private void setSettingsView() {
        SettingsView settingsView = new SettingsView();
        new SettingsPresenter(model, settingsView);
        view.getScene().setRoot(settingsView);
    }

}
