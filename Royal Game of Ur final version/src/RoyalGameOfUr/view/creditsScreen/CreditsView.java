package RoyalGameOfUr.view.creditsScreen;

import RoyalGameOfUr.view.Styles;
import RoyalGameOfUr.view.button.MuteButton;
import RoyalGameOfUr.view.button.ReturnButton;
import RoyalGameOfUr.view.button.SettingsButton;
import RoyalGameOfUr.view.setting.display.Effects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class CreditsView extends BorderPane {
    private ReturnButton returnButton;
    private MuteButton mute;
    private SettingsButton gear;
    private Text credits;
    private ImageView bannerView;
    private final Image image = new Image("/images/rgouv5.png");


    public CreditsView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        returnButton = new ReturnButton();
        mute = new MuteButton();
        gear = new SettingsButton();
        credits = new Text();
        bannerView = new ImageView(image);
    }

    private void layoutNodes() {
        this.setEffect(Effects.colorAdjust);
        this.setTop(addHBox());
        this.setCenter(addGPane());
        this.setPrefSize(500, 600);
        this.setStyle("-fx-background-image: url('/images/backgroundv4.jpg')");
    }


    private GridPane addGPane() {
        credits.setText("INSPIRED GAME\n" +
                "DEVELOPED BY\n" +
                "\n'ROYAL GAMERS'\n" +
                " PROGRAMMED BY\n" +
                " S.Assam\n" +
                " Q.Joseph\n" +
                " VB.Jef\n" +
                " & DESIGNED BY\n" +
                " S.Assam\n"+
                "TESTED BY\n" +
                "Q.Joseph\n" +
                "S.Assam\n" +
                "VB.Jef\n");
        credits.setStyle(Styles.CREDITS_STYLE);
        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setAlignment(Pos.CENTER);
        grid.add(bannerView, 0, 1);
        grid.add(credits, 0, 2);
        GridPane.setMargin(bannerView, new Insets(80, 50, 30, 30));
        GridPane.setMargin(credits, new Insets(0, 0, 70, 250));
        return grid;
    }
    private HBox addHBox() {
        HBox hbox = new HBox();
        HBox hbox2 = new HBox();
        hbox.setSpacing(522);
        hbox2.setSpacing(0);
        hbox2.setAlignment(Pos.TOP_RIGHT);
        hbox2.getChildren().addAll(gear, mute);
        hbox.getChildren().addAll(returnButton, hbox2);
        return hbox;
    }

    Button getGear() { return gear; }
    Button getReturnButton() {
        return returnButton;
    }
}
