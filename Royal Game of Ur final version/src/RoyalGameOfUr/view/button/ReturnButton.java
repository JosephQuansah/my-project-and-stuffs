package RoyalGameOfUr.view.button;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.transform.Transform;

public class ReturnButton extends Button {
    private final static String RETURN_BUTTON_STYLE = "-fx-padding:0;-fx-border-color: none; -fx-background-color: none;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.7), 0.0, 15.0, 0.0,  4.0);";
    private final static String RETURN_ON_HOVER = "-fx-padding: 0;-fx-border-color: none; -fx-background-color: none;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,1), 0.0, 15.0, 0.0,  1.0);";

    public ReturnButton() {
        layOut();
        eventHandlers();
        this.setFocusTraversable(false);
    }

    private void layOut() {
        this.setStyle(RETURN_BUTTON_STYLE);
        this.setGraphic(new ImageView(new Image("/images/returnbutton.png")));
    }

    private void eventHandlers() {
        this.setOnMousePressed(event -> {
            this.setStyle(RETURN_ON_HOVER);
            this.getTransforms().add(Transform.translate(0, 3));
        });
        this.setOnMouseReleased(event ->{
            this.setStyle(RETURN_BUTTON_STYLE);
            this.getTransforms().add(Transform.translate(0, -3));
        });
    }

}
