package RoyalGameOfUr.view.button;

import RoyalGameOfUr.view.setting.audio.SoundTrack;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class MuteButton extends Button {

    public final static String BUTTON_STYLE = "-fx-padding:0;-fx-border-color: none; -fx-background-color: none;";

    public MuteButton() {
        layOut();
        eventHandlers();
        this.setFocusTraversable(false);
    }

    private void layOut() {
        this.setStyle(BUTTON_STYLE);
        if (SoundTrack.isMuted()) {
            this.setGraphic(new ImageView(new Image("/images/mute.png")));
        } else {
            this.setGraphic(new ImageView(new Image("/images/unmute.png")));
        }
    }

    private void eventHandlers() {
        this.setOnMousePressed(event -> {
            controlAudio();
        });
    }

    private void controlAudio() {
        if (SoundTrack.isMuted()) {
            SoundTrack.unmuteTrack();
        } else {
            SoundTrack.muteTrack();
        }
        layOut();
    }
}
