package RoyalGameOfUr.view.button;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class SettingsButton extends Button {
    private final static String BUTTON_STYLE = "-fx-padding:0;-fx-border-color: none; -fx-background-color: none;";

    public SettingsButton() {
        layOut();
        eventHandlers();
        this.setFocusTraversable(false);
    }

    private void layOut() {
        this.setStyle(BUTTON_STYLE);
        this.setGraphic(new ImageView(new Image("/images/gear.png")));
    }

    private void eventHandlers() {
        this.setOnMousePressed(event -> {
            this.setGraphic(new ImageView(new Image("/images/gearrotate1.png")));
        });
        this.setOnMouseReleased(event -> {
            layOut();
        });
    }
}
