package RoyalGameOfUr.view.button;

import javafx.scene.control.Button;

public class MenuButton extends Button {
    private final static String MENU_STYLE = "-fx-text-fill:white; -fx-border-color: none; -fx-background-color: none; -fx-font-family: 'Comic Sans MS';-fx-font-size: 1.5em";
    private final static String MENU_ON_HOVER = "-fx-text-fill: #eab676;-fx-border-color: none; -fx-background-color: none; -fx-font-family: 'Comic Sans MS';-fx-font-size: 1.5em";

    public MenuButton(String s) {
        super(s);
        layOut();
        eventsHandlers();
        this.setFocusTraversable(false);
    }


    private void layOut() {
        this.setStyle(MENU_STYLE);
    }

    private void eventsHandlers() {
        this.setOnMouseEntered(mouseEvent -> this.setStyle(MENU_ON_HOVER));
        this.setOnMouseExited(mouseEvent -> this.setStyle(MENU_STYLE));
        this.setOnMouseEntered(mouseEvent -> this.setStyle(MENU_ON_HOVER));
        this.setOnMouseExited(mouseEvent -> this.setStyle(MENU_STYLE));
        this.setOnMouseEntered(mouseEvent -> this.setStyle(MENU_ON_HOVER));
        this.setOnMouseExited(mouseEvent -> this.setStyle(MENU_STYLE));
        this.setOnMouseEntered(mouseEvent -> this.setStyle(MENU_ON_HOVER));
        this.setOnMouseExited(mouseEvent -> this.setStyle(MENU_STYLE));
        this.setOnMouseEntered(mouseEvent -> this.setStyle(MENU_ON_HOVER));
        this.setOnMouseExited(mouseEvent -> this.setStyle(MENU_STYLE));
    }
}
