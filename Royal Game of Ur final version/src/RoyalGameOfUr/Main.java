package RoyalGameOfUr;

import RoyalGameOfUr.model.Model;
import RoyalGameOfUr.view.setting.audio.SoundTrack;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import RoyalGameOfUr.view.startScreen.StartScreenPresenter;
import RoyalGameOfUr.view.startScreen.StartScreenView;


public class Main extends Application {
    @Override
    public void start(Stage rgouStage) {
        SoundTrack.playBackGroundMusic();
        Model model = new Model ("Aby","Danny",50);
        StartScreenView view = new StartScreenView();
        StartScreenPresenter startPresenter = new StartScreenPresenter(model, view);
        Scene scene = new Scene(view);
        rgouStage.setScene(scene);
        startPresenter.addWindowEventHandlers();
        rgouStage.getIcons().add(new Image("/images/icon.png"));
        rgouStage.setTitle("Royal Game of Ur");
        rgouStage.setMinWidth(650);
        rgouStage.setMinHeight(500);
        rgouStage.setResizable(false);
        rgouStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
