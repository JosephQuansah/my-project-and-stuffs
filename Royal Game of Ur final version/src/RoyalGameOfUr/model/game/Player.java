package RoyalGameOfUr.model.game;


import RoyalGameOfUr.model.board.Disc;

import java.util.Scanner;

/** Represents an in-game player, it contains each player's discs, the player's name and the die to be rolled.
 * @author Assam Saadaoui
 * @version 1.5
 * @since 1.0
 */

public class Player {
    private final Disc[] discs;
    private final String name;
    private final Die die;

    /** Creates a player with the specified name and an array of discs.
     * @param  name A string containing the player's name.
     * @param  discs An array containing the player's discs.
     */
    public Player(String name, Disc[] discs) {
        die = new Die();
        this.discs = discs;
        this.name = name;
    }

    public Disc[] getDiscs() {
        return discs;
    }
    public String getName() {
        return name;
    }


    /** Gives turn to a player to roll a die.
     * @return A int containing the number of roll.
     */
    public int takeTurn() {
        //Initialize scanner
        Scanner scan = new Scanner(System.in);
        //Prompt user for roll
        char input = '-';
        while (input != 'R' && input != 'r') {
            System.out.print(name + "'s turn, (R|r) to roll: ");
            input = scan.next().charAt(0);
        }
        int roll = die.getNumberRolled();//getting an int from lookUp.Die class

        System.out.println(name + " rolled " + roll + ".");
        System.out.println();
        return roll;
    }

    @Override
    public String toString() {
        return name;
    }
}
