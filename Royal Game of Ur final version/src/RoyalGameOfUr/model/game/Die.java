package RoyalGameOfUr.model.game;

import java.util.Random;

/** Represents the die which to be rolled, it contains
 * the rolled number between 0 and 4,the max number a player can get from a roll.
 * @author VB.Jef
 * @version 1.5
 * @since 1.0
 */

public class Die {
    public static final int MAX_NUMBER = 4;
    private int numberRolled;
    private final Random random;

    /** Creates a die
     */
    public Die() {
        this.numberRolled = 1;
        this.random = new Random();
    }

    /** Sets the numberRolled to a random number between 0 and 4.
     */
    public void roll() {
        numberRolled = random.nextInt(MAX_NUMBER) + 1;
    }

    public int getNumberRolled() {
        return numberRolled;
    }
}
