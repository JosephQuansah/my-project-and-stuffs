package RoyalGameOfUr.model.game;

import RoyalGameOfUr.model.StaticValues;
import RoyalGameOfUr.model.board.Board;
import RoyalGameOfUr.model.board.Color;
import RoyalGameOfUr.model.board.Disc;

import java.util.*;


/** Represents the game logic containing the players and the board.
 * @author S.Assam
 * @version 1.5
 * @since 1.0
 */

public class Game {
    private final List<Player> players;
    private final Board board;

    //temp Player index
    private int playerIdx;

    //round Pawn indexes
    private int currentPlayer1DiscIdx;
    private int currentPlayer2DiscIdx;

    //in-game Players
    private final Player player1;
    private final Player player2;

    /** Creates a game with the specified players names.
     * @param  gamePlayers An array containing in-game players.
     */
    public Game(String[] gamePlayers) {
        Disc[][] discs = new Disc[gamePlayers.length][StaticValues.NUM_OF_DISCS];
        //Initialize the player's discs.
        for (int row = 0; row < discs.length; row++) {
            for (int col = 0; col < discs[row].length; col++) {
                if (row == 0) {
                    discs[row][col] = new Disc("I", Color.RED);
                } else {
                    discs[row][col] = new Disc("Z", Color.BLUE);
                }
            }
        }
        //Initialize the players.
        this.players = new ArrayList<Player>();
        for (int idx = 0; idx < gamePlayers.length; idx++) {
            players.add(new Player(gamePlayers[idx], discs[idx]));
        }
        System.out.println("-------------------------------------");
        for (Player player : players) {
            System.out.println(Arrays.toString(player.getDiscs()));
        }
        System.out.println("---------------------------------------");
        //Create the board
        this.board = new Board(players);
        //Game players
        player1 = players.get(0);
        player2 = players.get(1);
        //round Pawn indexes
        currentPlayer1DiscIdx = 0;
        currentPlayer2DiscIdx = 0;

    }


    /** Move a particular disc by a number 'value' of positions.
     * @return A boolean representing whether the disc reached the end or not.
     * @param  disc Containing the disc to be moved
     * @param value the roll number representing the number of moves
     */
    public boolean moveDisc(Disc disc, int value) {
//         todo: work in progress to update the discs positions based on the players paths
        int position = disc.getDiscPosition();
        int newPosition = position + value;
        if (newPosition >= StaticValues.LENGTH_OF_PLAYERPATH) {
            // End run: if disc reaches end
            disc.setDiscPosition(0);
            return true;
        } else {
            disc.setDiscPosition(newPosition);
            return false;
        }
    }


    /** Setting up the turn for next player.**/
    public void setUpNextTurn() {
        this.playerIdx++;
        if (this.playerIdx == 2) {
            this.playerIdx = 0;
        }
    }


    /** Checks the position of Round discs.
     * @return A boolean representing whether the next disc position is occupied .
     * @param currentDiscPos An int containing the current player's disc position
     * @param otherDiscPos An int containing the opponent's disc position
     */
    public boolean isPosOccupied(int currentDiscPos, int otherDiscPos) {
        if (currentDiscPos > 0 && otherDiscPos > StaticValues.START_OF_BATTLEGROUND) {
            return currentDiscPos == otherDiscPos;
        } else {
            return false;
        }
    }

    /** checks isPosOccupied, if true, means the current player's move lead to eating the other player's disc.
     * @param isPosOccupied A boolean containing the state of the discs next position
     */
    public void isEaten(boolean isPosOccupied) {
        int roll;
        if (isPosOccupied) {
            System.out.println(getOtherPlayer().getName() + "'s Disc was sent back home");
            //re-set the turn for currentPlayer
            roll = getCurrentPlayer().takeTurn();
            this.moveDisc(getCurrentPlayerDisc(), roll);
        }
    }

    public boolean isGameOver(int currDiscIdx) {
        return currDiscIdx >= 7;
    }

    //Used to set the other Player's disc pos (mainly for isEaten method)
    public void setOtherDiscPos(int val) {
        getOtherPlayerDisc().setDiscPosition(val);
    }

    //Used to update the index of the playable Discs when saved
    public void setCurrentPlayer1DiscIdx(int currentPlayer1DiscIdx) {
        this.currentPlayer1DiscIdx = currentPlayer1DiscIdx;
    }

    public void setCurrentPlayer2DiscIdx(int currentPlayer2DiscIdx) {
        this.currentPlayer2DiscIdx = currentPlayer2DiscIdx;
    }

    //Getters
    //Get current player of a certain round, and the opponent(otherPlayer)
    public Player getCurrentPlayer() {
        //current round player
        return players.get(playerIdx);
    }

    /** gets the opponent of each player on each turn.
     * @return Player that represents the opponent
     */
    public Player getOtherPlayer() {
        Player otherPlayer;
        if (playerIdx == 1) {
            otherPlayer = players.get(0);
        } else {
            otherPlayer = players.get(1);
        }
        return otherPlayer;
    }

    /** Get the discs of the in-game Players.
     * @return Disc of the current player
     */
    public Disc getCurrentPlayerDisc() {
        Disc currDisc = null;
        if (getCurrentPlayer() == player1) {
            currDisc = player1.getDiscs()[this.currentPlayer1DiscIdx];
        } else if (getCurrentPlayer() == player2) {
            currDisc = player2.getDiscs()[this.currentPlayer1DiscIdx];
        }
        return currDisc;
    }

    public Disc getOtherPlayerDisc() {
        Disc otherDisc = null;
        if (getCurrentPlayer() == player1) {
            otherDisc = player2.getDiscs()[this.currentPlayer2DiscIdx];
        } else if (getCurrentPlayer() == player2) {
            otherDisc = player1.getDiscs()[this.currentPlayer1DiscIdx];
        }
        return otherDisc;
    }

    //Get the in-game players
    public Player getPlayer1() {
        return player1;
    }
    public Player getPlayer2() {
        return player2;
    }

    //Get the index of Player's discs
    public int getCurrentPlayer1DiscIdx() {
        return currentPlayer1DiscIdx;
    }
    public int getCurrentPlayer2DiscIdx() {
        return currentPlayer2DiscIdx;
    }


}
