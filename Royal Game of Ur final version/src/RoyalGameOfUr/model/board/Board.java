package RoyalGameOfUr.model.board;

import RoyalGameOfUr.model.StaticValues;
import RoyalGameOfUr.model.game.Player;
import java.util.*;
/** Represents the board, it contains the players, rosettes and the players paths.
 * @author S.Assam
 * @author VB.Jef
 * @version 1.5
 * @since 1.0
 */

public class Board {

    private final List<Player> players;
    private List<Rosette> rosettes;

    //Players paths
    private final List<Integer> player1Path = Arrays.asList(2, 1, 9, 10, 11, 12, 13, 14, 15, 16, 8, 7, 6, 5);
    private final List<Integer> player2Path = Arrays.asList(17, 16, 9, 10, 11, 12, 13, 14, 15, 16, 23, 22, 21, 20);

    //Board
    private final int[][] gameBoard;


    /** Creates a board containing in-game player's discs.
     * @param  players An array containing the in-game players.
     */
    public Board(List<Player> players) {

        //Initialize the discs positions
        this.players = new ArrayList<>(players);
        for (Player player : players) {
            for (Disc disc : player.getDiscs()) {
                disc.setDiscPosition(0);
            }
        }
        gameBoard = new int[StaticValues.ROWS][StaticValues.COLS];
        for (int row = 0; row < StaticValues.ROWS; row++) {
            for (int col = 0; col < StaticValues.COLS; col++) {
                if (row == 0) {
                    gameBoard[row][col] = col + 1;
                }
                if (row == 1) {
                    gameBoard[row][col] = col + 9;
                }
                if (row == 2) {
                    gameBoard[row][col] = col + 16;
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int row = 0; row < StaticValues.ROWS; row++) {
            for (int col = 0; col < StaticValues.COLS; col++) {
                StringBuilder pl = new StringBuilder();
                StringBuilder fl = new StringBuilder();
                boolean occupied = false;
                int playerPathPos = 0;
                for (Player player : players) {
                    if (player == players.get(0)) {
                        for (Disc disc : players.get(0).getDiscs()) {
                            if (disc.getDiscPosition() != 0) {
                                playerPathPos = player1Path.get(disc.getDiscPosition() - 1);
                                if (playerPathPos == gameBoard[row][col]) {
                                    occupied = true;
                                    pl.append("(").append(disc).append(") | ");
                                }
                            }
                        }
                    }
                    if (player == players.get(1)) {
                        for (Disc disc : players.get(1).getDiscs()) {
                            if (disc.getDiscPosition() != 0) {
                                playerPathPos = player2Path.get(disc.getDiscPosition() - 1);
                                if (playerPathPos == gameBoard[row][col]) {
                                    occupied = true;
                                    pl.append("(").append(disc).append(") | ");
                                }
                            }
                        }
                    }
                }
                if (occupied) {
                    //If at least one player occupies the location, then print those players
                    pl.append(" ");
                    sb.append(pl);
                }
                else {
                    //Else,  print the position number
                    sb.append(gameBoard[row][col]).append("\t\t|");
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }

}


