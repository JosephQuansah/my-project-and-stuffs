package RoyalGameOfUr.model.board;

import RoyalGameOfUr.model.StaticValues;

/** Represents the movable disc, it contains the disc position, and the colour.
 * @author S.Assam
 * @author Q.Joseph
 * @version 1.5
 * @since 1.0
 */

public class Disc {
    private int discPosition;
    private final String layout;
    private final Color color;


    /** Creates a disc with the layout and the colour.
     * @param  layout shape of disc to be displayed.
     * @param  color An enum colour of disc.
     */
    public Disc(String layout, Color color) {
        this.layout = layout;
        this.color = color;
    }

    /** Move a particular disc by a number 'value' of positions.
     * @return A boolean representing whether the disc reached the end or not.
     * @param  disc Containing the disc to be moved
     * @param value An int contains the roll number representing the number of moves
     */
    public boolean moveDisc(Disc disc, int value) {
        int position = disc.getDiscPosition();
        int newPosition = position + value;
        if (newPosition >= StaticValues.LENGTH_OF_PLAYERPATH) {
            // End run: if disc reaches end
            disc.setDiscPosition(0);
            return true;
        } else {
            disc.setDiscPosition(newPosition);
            return false;
        }
    }



    public int getDiscPosition() {
        return discPosition;
    }

    public void setDiscPosition(int discPosition) {
        this.discPosition = discPosition;
    }

    @Override
    public String toString() {
        return layout;
    }
}
