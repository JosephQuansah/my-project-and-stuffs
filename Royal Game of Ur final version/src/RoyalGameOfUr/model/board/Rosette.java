package RoyalGameOfUr.model.board;

/** Represents safe spots on the board or battleground, it contains the rosette position.
 * @author S.Assam
 * @author VB.Jef
 * @version 1.5
 * @since 1.0
 */

public class Rosette {
    private int rosettePosition;

    /** Creates a disc with the layout and the colour.
     * @param  rosettePosition An int which contains the position of the rosette on the map.
     */
    public Rosette(int rosettePosition) {
        this.rosettePosition = rosettePosition;
    }

    public int getRosettePosition() {
        return rosettePosition;
    }

    public void setRosettePosition(int discPosition) {
        this.rosettePosition = discPosition;
    }

}
