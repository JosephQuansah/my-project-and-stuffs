package RoyalGameOfUr.model;

import java.util.*;

public class Model {
    private final Random random = new Random();
    private int randomNumber;

    private String player2Name;
    private String player1Name;

    private int row;
    private int row2;

    private int oldPos;
    private int newPos;

    private int score;
    private int score2;

    private int discSaved;

    private int playerId;

    private int number;

    private String color;

    public Model(){}

    public Model(String player1Name,String player2Name, int score) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
        this.score = score;
        this.randomNumber = this.random.nextInt(3) + 1;
    }

    public void setPlayer1Name(String name) {
        this.player1Name = name;
    }
    public String getPlayer1Name() {
        return player1Name;
    }
    public void setPlayer2Name(String name2) {
        this.player2Name = name2;
    }
    public String getPlayer2Name() {
        return player2Name;
    }


    public void scorePlayer() {
        number += randomNumber;
        if ( number > 2) {
            score += 15;
        } else {
            score -= 15;
        }
    }
    public int getScore() {
        return score;
    }


    public int getPlayer2Roll() {
        return randomNumber;
    }
    public int getPlayer1Roll() {
        return randomNumber;
    }


    public int getPlayerId() {
        if (getPlayer1Name() != null) {
            playerId = 1;
        }
        if (getPlayer2Name() != null) {
            playerId = 2;
        }
        return playerId;
    }


    //this should be getting the disc positions
    public void setPos(int oldPos) {
        this.oldPos = oldPos;
    }
    public void setPos2(int oldPos) {
        this.newPos = oldPos;
    }
    public int getOldPos() {
        return oldPos;
    }
    public int getNewPos() {
        return newPos;
    }


    public int getDiscSaved() {
        return discSaved;
    }


    //this should be getting the disc saved
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }



    public int getRow2() {
        return row2;
    }

    public int getRow() {
        return row;
    }
    public void setRow(int roll) {
        if (roll < 5 || roll > 13) {
            row = 165;
        } else {
            row = 255;
        }
    }
    public void setRow2(int roll) {
        if (roll < 5 || roll > 13) {
            row2 = 340;
        } else {
            row2 = 255;
        }
    }

    /*Use a method getDiscMoved() and then use it without the loop and all that you need is the checks*/
    public void protectDisc(int j) {

    }

    /**
     * The finish method is a void method
     * This method only pops up when the player finished the race
     * The player is also given some points only if he or she finishes
     *
     * @Author: Joseph Quansah Kotei
     */

    public void finish(int roll) {
        if (roll > 14 ) { // check to see if dice rolled is more than 3
            discSaved += 1;
            System.out.println("1 Your score is : " + score + " and you saved " + getDiscSaved());
        }
    }
    public void finish2(int roll) {
        if (roll > 14 ) {// check to see if dice rolled is more than 3
            discSaved += 1;
            score = score + 50;
            System.out.println("2 Your score is : " + score + " and you saved " + getDiscSaved());
        }
    }

    public void setScore2(){
        number += randomNumber;
        if ( number > 2) {
            score2 += 15;
        } else {
            score2 -= 15;
        }
    }

    public int getScore2() {
        return score2;
    }
}


