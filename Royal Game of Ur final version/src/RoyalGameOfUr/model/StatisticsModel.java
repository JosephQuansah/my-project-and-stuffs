package RoyalGameOfUr.model;

import java.sql.*;
import java.util.Random;

public class StatisticsModel {

   private final Model model = new Model();
//    public void dataBase() {
//        try {
//            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@rgoudb_medium?TNS_ADMIN=C:/Users/empha" +
//                            "/Desktop/projects/Vsprojects/wallet_RGOUDB",
//                    "GAME_OF_UR",
//                    "GameofUr_1111");
//            Statement stmt = con.createStatement();
//
//    stmt.executeUpdate("Insert into Players values("+ model.getPlayerId() +",'" +model.getPlayer1Name()+"')");
//            ResultSet rs = stmt.executeQuery("Select * from Players");
//            while (rs.next()) {
//                System.out.println(
//                        rs.getString(1) + " " +
//                                rs.getString(2)
//                              );
//            }
//
//    stmt.executeUpdate("Insert into Games values(" + model.getOldPos() + "," +1+ "," +model.getOldPos()+ "," +model
//    .getNewPos() + ",SYSTEM_TIME," +model.getDiscSaved()+ ")");
//            ResultSet rs1 = stmt.executeQuery("Select * from Games");
//            while (rs1.next()) {
//                System.out.println(
//                        rs1.getString(1) + " " +
//                                rs1.getString(2) + " " +
//                                rs1.getString(3) + " " +
//                                rs1.getString(4) + " " +
//                                rs1.getString(5) + " " +
//                                rs1.getString(6));
//            }
//
//            stmt.executeUpdate("Insert into Moves values(" + 1 + "," + model.getPlayerId() + ",SYSTEM_TIME ,SYSTEM_TIME," + model
//                    .getScore() + ")");
//            ResultSet rs2 = stmt.executeQuery("Select * from Moves");
//            while (rs2.next()) {
//                System.out.println(
//                        rs2.getString(1) + " " +
//                                rs2.getString(2) + " " +
//                                rs1.getString(3) + " " +
//                                rs1.getString(4) + " " +
//                                rs1.getString(5));
//            }
//            con.close();
//        } catch (Exception e) {
//           e.printStackTrace();
//        }
//    }
    private String name;
    private int score;
    private int score2;
    private int numberRolled;
    private int discSaved;
    private String opponent;

    public StatisticsModel(String name, int score, int score2, int numberRolled, int discSaved, String opponent){
        this.name = name;
        this.score = score;
        this.score2 = score2;
        this.numberRolled = numberRolled;
        this.discSaved = discSaved;
        this.opponent = opponent;
    }

    public String getName(){
        return name;
    }

    public String getOpponent(){
        return opponent;
    }


    public int getScore(){
        return score;
    }

    public int getScore2(){
        Random r = new Random();
        score2 = r.nextInt(4)+11;
        return score2+=2;
    }

    public int getDiscSaved(){
        return discSaved;
    }

    public int getNumberRolled(){
        return numberRolled;
    }

    public void dataBase() {
        try {
            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@jbwork_medium?TNS_ADMIN=C:/Users/empha/Desktop/projects/Vsprojects/wallet_JBWORK",
                    "JDBWORK", "JeP2020-2023A");
            Statement statement = con.createStatement();
//            statement.execute("CREATE TABLE GAME("
//                    + "playerName Varchar2(20), score number(4), opponentName Varchar2(20), score2 number(4), numberRolled number(4),discsSaved number(4))");
            statement.executeUpdate("Insert into GAME values('" +
                    getName()+ "'," + getScore() + ",'" + getOpponent() + "'," + getScore2() + "," + getNumberRolled() + "," + getDiscSaved() + ")");
            ResultSet rs = statement.executeQuery("Select * from game");
            while (rs.next()) {
                System.out.println(
                        rs.getString(1) + " " +
                                rs.getString(2) + " " +
                                rs.getString(3) + " " +
                                rs.getString(4) + " " +
                                rs.getString(5) + " " +
                                rs.getString(6));
            }
            con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
