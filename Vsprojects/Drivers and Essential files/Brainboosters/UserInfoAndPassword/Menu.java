package UserInfoAndPassword;

import java.util.Scanner;

public class Menu {
    Scanner scan = new Scanner(System.in);

    public void Menu1() {
        System.out.println("" +
                "\n||==============================================||" +
                "\n||                                              ||" +
                "\n||                                              ||" +
                "\n||           =>>REVIEW YOUR DATA<<=             ||" +
                "\n||                                              ||" +
                "\n||                 LOAD DATA                    ||" +
                "\n||                                              ||" +
                "\n||               SAVE YOUR DATA                 ||" +
                "\n||                                              ||" +
                "\n||                                              ||" +
                "\n||==============================================||");
    }

    public void Menu2() {
        System.out.println("" +
                "\n||==============================================||" +
                "\n||                                              ||" +
                "\n||                                              ||" +
                "\n||              REVIEW YOUR DATA                ||" +
                "\n||                                              ||" +
                "\n||              =>>LOAD DATA<<=                 ||" +
                "\n||                                              ||" +
                "\n||               SAVE YOUR DATA                 ||" +
                "\n||                                              ||" +
                "\n||                                              ||" +
                "\n||==============================================||");
    }

    public void Menu3() {
        System.out.println("" +
                "\n||==============================================||" +
                "\n||                                              ||" +
                "\n||                                              ||" +
                "\n||              REVIEW YOUR DATA                ||" +
                "\n||                                              ||" +
                "\n||                 LOAD DATA                    ||" +
                "\n||                                              ||" +
                "\n||            =>>SAVE YOUR DATA<<=              ||" +
                "\n||                                              ||" +
                "\n||                                              ||" +
                "\n||==============================================||");
    }

    public void Load() {
        SaveandLoad sl = new SaveandLoad();
        System.out.printf("%s %5s %5s", sl.getName(), sl.profession(), sl.userInfo());
    }

    public void save() {
        System.out.println("Your data is safely saved");
    }

    public void instructions() {
        System.out.println("\nChoose from the menu page with the use of the following keys\n " +

                "press i to select the first menu\n" +
                "press k to select the second menu\n" +
                "press m to select the third menu\n" +
                "press c to enter your data from the menu\n" +
                "press s to save your data");
    }

    public void choice() {
        Menu menu = new Menu();
        Mainmenu mainmenu = new Mainmenu();
        String select = scan.nextLine();

        switch (select) {
            case "i":
                menu.Menu1();
                break;
            case "k":
                menu.Menu2();
                break;
            case "m":
                menu.Menu3();
                break;
            case "f":
                menu.instructions();
                break;
            case "c":
                menu.Load();
                break;
            case "s":
                menu.save();
                break;
            case "o":
                mainmenu.stop();
                break;
        }
    }

    public static void main(String[] args) {
        Mainmenu mainmenu = new Mainmenu();
        mainmenu.Instructions();
    }
}
//}
