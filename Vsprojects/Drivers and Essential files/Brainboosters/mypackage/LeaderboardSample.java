package mypackage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;


public class LeaderboardSample {
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        System.out.println("Below are the questions you have to answer to get more points ");
        int counter = 1;
        int constant = 4;
        int answer = 10;
        int points = 0;
        int score = 0;
        int rank = 0;
//        MyTimer.start();
        while (counter <= 5) {
            System.out.println(counter + "]" + " Complete the questions below to gain points: ");
            System.out.println("[_] + " + constant + " = " + answer);
            System.out.print("Enter your answer here: ");
            if (scan.nextInt() + constant == answer) {
                points += 4;
                System.out.print("You are correct! " + "You have " + points + "points \n");
                System.out.println(" ");
            } else {
                final int i = points -= 4;
                System.out.println("You are wrong!!!" + "You have " + i + " points");
            }
            counter++;
            answer += 5;
            constant += 3;

        }
        if (points <= 4) {
            rank = 5;
        } else if (points <= 8) {
            rank = 4;
        } else if (points <= 12) {
            rank = 3;
        } else if (points <= 16) {
            rank = 2;
        } else {
            rank = 1;
        }



        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@dbstudent_medium?TNS_ADMIN=C:/KDG/AIT/Data&AI/wallet_DBSTUDENT", "DBSTUDENT", "Student_1111");
        score = points;
        try {
            Statement statement = con.createStatement();

            statement.execute("CREATE TABLE INT_HIGH_SCORE (username VARCHAR2(20), rank NUMBER(10), score NUMBER(10), time TIMESTAMP);");

            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("username ");
            String username = input.readLine();
            System.out.println("time completed ");
            String time = input.readLine();


            String insert = "Insert into INT_HIGH_SCORE values (" + "'" + username + "','" + rank +
                    "','" + score + "','" + time + "')";

            System.out.println(insert);
            int inserted = statement.executeUpdate(insert);
            if (inserted > 0) {
                System.out.println("Successfully inserted " + inserted + "row.");
            }

            ResultSet resultSet = statement.executeQuery("Select * from INT_HIGH_SCORE where rank between 5 and 0;");

            while (resultSet.next()) {
                System.out.print(resultSet.getString(1) + " "
                        + resultSet.getString(2) + " " +
                        resultSet.getInt(3) + " ");
            }

            statement.close();
            resultSet.close();
            con.close();

        } catch (Exception e) {
            System.out.println("error generated!!");
        }

    }
}

